﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.IO;
//
using Foregate.Redrow.Models;

namespace Foregate.Redrow.Controllers
{
    public class CategoryController : Controller
    {
        RedrowDataContext rdc = new RedrowDataContext();
        protected override void Dispose(bool disposing)
        {
            
            base.Dispose(disposing);
        }
        // **************************************
        // URL: /Category/Category
        // **************************************
        [Authorize(Roles = "Administrator")]
        public ActionResult Category(int? id)
        {
            @ViewBag.Title = "Administartion Category";
            CategoryViewData viewData = new CategoryViewData();
            viewData.Category = new Category();
            if (id != null && id != int.MinValue)
            {
                viewData.Category = rdc.GetCategory(id.Value);
            }
            viewData.Catalogs = rdc.GetCatalogs();
            viewData.Categories = rdc.GetCategories();
            return View(viewData);
        }
        [HttpPost, Authorize(Roles = "Administrator")]
        public ActionResult Category(CategoryViewData model)
        {
            if (ModelState.IsValid)
            {
                string changeDirectory = string.Empty;
                Category category = new Category();
                if (model.Category.CategoryId > 0)
                {
                    category = rdc.GetCategory(model.Category.CategoryId);
                    category.UpdatedDate = DateTime.Now;
                    if (model.Category.Name != category.Name)
                    {
                        changeDirectory = category.FolderName;
                    }
                    category.Name = model.Category.Name;
                    category.PropertyPortal = model.Category.PropertyPortal;
                    category.AdvertisingApproved = model.Category.AdvertisingApproved;
                    category.SocialMedia = model.Category.SocialMedia;
                    category.CatalogId = model.Category.CatalogId;
                    rdc.SubmitChanges();
                    //Now if the Category name has changed change the directory name to match
                    if (changeDirectory != string.Empty)
                    {
                        if (Directory.Exists(changeDirectory) == true)
                            Directory.Move(changeDirectory, Path.Combine(category.Catalog.FolderName, category.Name));
                    }
                }
                else
                {
                    category = new Category();
                    category.CreatedDate = DateTime.Now;
                    category.UpdatedDate = DateTime.Now;
                    category.Name = model.Category.Name;
                    category.PropertyPortal = model.Category.PropertyPortal;
                    category.AdvertisingApproved = model.Category.AdvertisingApproved;
                    category.SocialMedia = model.Category.SocialMedia;
                    category.CatalogId = model.Category.CatalogId;
                    rdc.Categories.InsertOnSubmit(category);
                    rdc.SubmitChanges();

                    //Create the folder for this category
                    string categoryFolder = category.FolderName;

                    if (System.IO.Directory.Exists(categoryFolder) == false)
                        System.IO.Directory.CreateDirectory(categoryFolder);
                }
                return RedirectToAction("Category", "Category");
            }
            model.Catalogs = rdc.GetCatalogs();
            model.Categories = rdc.GetCategories();
            return View(model);
        }
        [Authorize]
		public ActionResult Delete(int id)
		{
            Category category = rdc.GetCategory(id);
            string folderName = category.FolderName;
            rdc.Categories.DeleteOnSubmit(category);

			//delete files (and also thumbnails that are in a different folder)
            //foreach (Image CategoryImage in category.Images)
            //{
            //    CategoryImage.DeleteFiles();
            //    rdc.Images.DeleteOnSubmit(CategoryImage);
            //}

			if (Directory.Exists(folderName))
				Directory.Delete(folderName, true);

			rdc.SubmitChanges();

            return RedirectToAction("Category", "Category");
		}
        // **************************************
        // URL: /Category/Catalog
        // **************************************
        [Authorize(Roles = "Administrator")]
        public ActionResult Catalog(int? id)
        {
            @ViewBag.Title = "Administartion Catalog";
            CatalogListViewData viewData = new CatalogListViewData();
            viewData.Catalog = new Catalog();
            if (id != null && id != int.MinValue)
            {
                viewData.Catalog = rdc.GetCatalogById(id.Value);
            }
            viewData.Catalogs = rdc.GetCatalogs();
            return View(viewData);
        }
        [HttpPost, Authorize(Roles = "Administrator")]
        public ActionResult Catalog(CatalogListViewData model)
        {
            if (ModelState.IsValid)
            {
                Catalog catalog = new Catalog();
                if (model.Catalog.CatalogId > 0)
                {
                    catalog = rdc.GetCatalogById(model.Catalog.CatalogId);
                    catalog.UpdatedDate = DateTime.Now;
                    catalog.Name = model.Catalog.Name;
                    catalog.AllUsers = model.Catalog.AllUsers;
                    //Note the directory name remains the same as the original catalog name 
                    //and is stored on the database in table catalog field directory
                    rdc.SubmitChanges();
                }
                else
                {
                    catalog = new Catalog();
                    catalog.CreatedDate = DateTime.Now;
                    catalog.UpdatedDate = DateTime.Now;
                    catalog.Name = model.Catalog.Name;
                    catalog.Directory = model.Catalog.Name;
                    catalog.AllUsers = model.Catalog.AllUsers;
                    rdc.Catalogs.InsertOnSubmit(catalog);
                    rdc.SubmitChanges();

                    if (Directory.Exists(Properties.Settings.Default.ImagesFolder + Path.DirectorySeparatorChar + catalog.Name) == false)
                        Directory.CreateDirectory(Properties.Settings.Default.ImagesFolder + Path.DirectorySeparatorChar + catalog.Name);
                }
                return RedirectToAction("Catalog","Category" );
            }
            model.Catalogs = rdc.GetCatalogs().OrderBy(p => p.Name).ToList();

            return View(model);
        }
        [Authorize(Roles = "Administrator")]
        public ActionResult DeleteCat(int id)
        {
            Catalog catalog = rdc.GetCatalogById(id);
            if (catalog.Categories.Count == 0)
            {
                rdc.Catalogs.DeleteOnSubmit(catalog);
                rdc.SubmitChanges();
            }
            else
            {
                return RedirectToAction("Error", "Home", new { error = "Cannot delete catalog " + catalog.Name.ToString() + " as categories exist. Delete categories first then try again" });
            }
            return RedirectToAction("Catalog","Category" );
        }
        // **************************************
        // URL: /Category/SubCategory
        // **************************************
        [Authorize(Roles = "Administrator")]
        public ActionResult SubCategory(int? id)
        {
            SubCategoryViewData viewData = new SubCategoryViewData();
            viewData.SubCategory = new SubCategory();
            if (id != null && id != int.MinValue)
            {
                viewData.SubCategory = rdc.GetSubCategory(id.Value);
            }
            viewData.SubCategories = rdc.GetSubCategories().OrderBy(p => p.Name).ToList();
            ViewBag.Category = rdc.GetCategories();
            return View(viewData);
        }
        [HttpPost, Authorize(Roles = "Administrator")]
        public ActionResult SubCategory(SubCategoryViewData model)
        {
            if (ModelState.IsValid)
            {
                SubCategory subCategory = new SubCategory();
                if (model.SubCategory.SubCategoryId > 0)
                {
                    subCategory = rdc.GetSubCategory(model.SubCategory.SubCategoryId);
                    subCategory.Name = model.SubCategory.Name;
                    subCategory.CategoryId = model.SubCategory.CategoryId;
                    rdc.SubmitChanges();
                }
                else
                {
                    subCategory = new SubCategory();
                    subCategory.Name = model.SubCategory.Name;
                    subCategory.CategoryId = model.SubCategory.CategoryId;
                    rdc.SubCategories.InsertOnSubmit(subCategory);
                    rdc.SubmitChanges();

                    //Create the folder for this category
                    // No longer required as Sub Category files go in the category folder
                    //string subCategoryFolder = subCategory.FolderName;

                    //if (System.IO.Directory.Exists(subCategoryFolder) == false)
                    //    System.IO.Directory.CreateDirectory(subCategoryFolder);
                }
                return RedirectToAction("Category", "Category");
            }
            model.Categories = rdc.GetCategories().OrderBy(p => p.Name).ToList();
            return View(model);
        }
        //[Authorize(Roles = "Administrator")]
        //public ActionResult SubCatagoryDelete(int id)
        //{
        //    SubCategory subCategory = rdc.GetSubCategory(id);
        //    string folderName = subCategory.FolderName;
        //    rdc.SubCategories.DeleteOnSubmit(subCategory);

        //    //Delete Images from image table
        //    foreach (Image CategoryImage in subCategory.ImageCategories.Where(p=>p.SubCategoryId == id).Select(p=>p.Image).ToList())
        //    {
        //        rdc.Images.DeleteOnSubmit(CategoryImage);
        //        rdc.SubmitChanges();
        //    }
        //    // Delete sub catagory folder
        //    if (Directory.Exists(folderName))
        //        Directory.Delete(folderName, true);

        //    rdc.SubmitChanges();

        //    return RedirectToAction("SubCategory", "Category");
        //}
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult JsonSubCategory(string id)
        {
            //List<EarningTier> earningTiers = new List<EarningTier>();
            if (!string.IsNullOrEmpty(id))
            {

                var subCategory = rdc.GetSubCategoriesByCategory(int.Parse(id)).Select(x => new
                {
                    Value = x.SubCategoryId.ToString(),
                    Text = x.Name
                }).ToList();
                subCategory.Add(new { Value = "0", Text = "None" });

                return Json(subCategory.OrderBy(p => p.Value), JsonRequestBehavior.AllowGet);
            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }
    }
}
