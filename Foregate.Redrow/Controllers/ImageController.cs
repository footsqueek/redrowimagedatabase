﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
//
using Foregate.Redrow.Models;
using Foregate.Redrow.Helpers;
using Foregate.Redrow.Properties;
//
namespace Foregate.Redrow.Controllers
{
    [HandleError]
    [ValidateOnlyIncomingValues]
    public class ImageController : Controller
    {
        RedrowDataContext RDC = new RedrowDataContext();
        
        /// <summary>
        /// Import CSV file of images and stuff
        /// </summary>
        /// 
        //[Authorize]
        //public ActionResult Convert()
        //{
        //    @ViewBag.Title = "Image convert";
        //    CatalogListViewData viewData = new CatalogListViewData();
        //    List<FileInfo> files = new System.IO.DirectoryInfo(Path.Combine(Properties.Settings.Default.ThumbsAndPreviewsFolder, "Preview")).GetFiles("*.png", SearchOption.TopDirectoryOnly).OrderByDescending(p => p.CreationTime).ToList();
        //    string path = Path.Combine(Properties.Settings.Default.ThumbsAndPreviewsFolder, "Convert");
        //    viewData.files = new List<FileInfo>();
        //    foreach (FileInfo file in files)
        //    {
        //        string fileName = Path.GetFileName(file.FullName);

        //        if (!System.IO.File.Exists(Path.Combine(path, fileName)))
        //        {
        //            viewData.files.Add(file);
        //        }
        //    }

        //    System.IO.File.AppendAllText(Server.MapPath("~/App_Data/") + "ConvertErrors.log", "\r\n Number of files to convert : " + viewData.files.Count());

        //    viewData.Upload = System.IO.File.ReadAllText(Server.MapPath("~/App_Data/") + "ConvertErrors.log");
        //    viewData.RecordCount = viewData.files.Count();
        //    return View(viewData);
        //}
        //[Authorize, HttpPost]
        //public ActionResult Convert(FormCollection model)
        //{
        //    ImageImport ii = new ImageImport();
        //    ii.RootFolder = Server.MapPath("~/App_Data/");
        //    ii.Log = "ConvertErrors.log";
        //    ii.DirectoryToImport = new System.IO.DirectoryInfo(Path.Combine( Properties.Settings.Default.ThumbsAndPreviewsFolder,"Preview"));
        //    HttpContext.Application["ImageImport"] = ii;

        //    var taskId = Guid.NewGuid();
        //    tasks.Add(taskId, "Processed " + ii.FilesProcessed + " of " + ii.TotalFiles + ". Converted " + ii.AddedCount + ". Working on file: " + ii.WorkingOn);

        //    List<FileInfo> allFiles = new System.IO.DirectoryInfo(Path.Combine(Properties.Settings.Default.ThumbsAndPreviewsFolder, "Preview")).GetFiles("*.png", SearchOption.TopDirectoryOnly).OrderByDescending(p => p.CreationTime).ToList();
        //    string path = Path.Combine(Properties.Settings.Default.ThumbsAndPreviewsFolder, "Convert");
        //    List<FileInfo> selectedFiles = new List<FileInfo>();
        //    foreach (FileInfo file in allFiles)
        //    {
        //        string fileName = Path.GetFileName(file.FullName);

        //        if (!System.IO.File.Exists(Path.Combine(path, fileName)))
        //        {
        //            selectedFiles.Add(file);
        //        }
        //    }
        //    ii.TotalFiles = selectedFiles.Count();
        //    ii.LogError("");
        //    ii.LogError("Started conversion of " + ii.TotalFiles + " files");
        //    Task.Factory.StartNew(() =>
        //    {
        //        foreach (FileInfo file in selectedFiles)
        //        {
        //            string fileName = Path.GetFileName(file.FullName);
        //            if (!System.IO.File.Exists(Path.Combine(path, fileName)))
        //            {
        //                ii.FilesProcessed++;
        //                if (!ii.ConvertFile(Path.Combine(Properties.Settings.Default.ThumbsAndPreviewsFolder, "Preview", fileName)))
        //                {
        //                    ii.LogError(fileName + " not converted");
        //                }
        //                else
        //                {
        //                    ii.LogError(fileName + " converted");
        //                    ii.AddedCount++;
        //                }

        //                tasks[taskId] = "Processed " + ii.FilesProcessed + " of " + ii.TotalFiles + ". Added " + ii.AddedCount + " and updated " + ii.UpdatedCount + ". Working on file: " + ii.WorkingOn;
        //                Thread.Sleep(2000);
        //            }
        //        }
        //        tasks.Remove(taskId);
        //    });
        //    return Json(taskId);
        //}
        //public ActionResult ConvertProgress(Guid id)
        //{
        //    return Json(tasks.Keys.Contains(id) ? tasks[id] : "");
        //}
        [Authorize]
        public ActionResult Delete(int id, string requestUrl)
        
        {

            Response.Write("Started");

            try
            {
                Image image = RDC.GetImage(id);
                // Check if imagedetail is in more than one category
                List<Image> images = RDC.GetImagesByImageDetail(image.ImageDetailId);


              

               
               

                List<BasketImage> BasketImages = RDC.GetBasketImageByImageId(id);
                foreach (BasketImage BI in BasketImages)
                {
                    RDC.BasketImages.DeleteOnSubmit(BI);
                    RDC.SubmitChanges();
                }



                RDC.Images.DeleteOnSubmit(image);
                RDC.SubmitChanges();

                //delete image from disk
                if (images.Count == 1)
                {


                    RDC.ImageDetails.DeleteOnSubmit(images[0].ImageDetail);
                    RDC.SubmitChanges();
                    image.DeleteFiles();
                }

               // RDC.ImageDetails.DeleteOnSubmit(image.ImageDetail);
                RDC.SubmitChanges();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { error = ex.Message });
            }
            if (requestUrl == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return Redirect(requestUrl);
        }
        //[HttpPost, Authorize]
        //public ActionResult MoveImages(FormCollection model)
        //{
        //    //get checked images
        //    int ImageId;
        //    if (model["Process"] == "Move")
        //    {
        //        string srcFileName;
        //        if (int.Parse(model["SubCategoryId"]) != 0)
        //        {
        //            SubCategory MoveToSubCategory = RDC.GetSubCategory(int.Parse(model["SubCategoryId"]));
        //            foreach (string key in model.Keys)
        //            {
        //                //get checkbox
        //                if (key.StartsWith("chkSelect_"))
        //                {
        //                    if (model[key].StartsWith("true"))
        //                    {
        //                        ImageId = int.Parse(key.Substring(10));

        //                        //move these images
        //                        Image img = RDC.GetImage(ImageId);

        //                        if (img.SubCategoryId != MoveToSubCategory.SubCategoryId)
        //                        {
        //                            srcFileName = img.Location;

        //                            img.SubCategory = MoveToSubCategory;

        //                            RDC.SubmitChanges();

        //                            //move the physical file
        //                            if (System.IO.File.Exists(srcFileName))
        //                            {
        //                                //create the category folder if it doesn't exist
        //                                if (Directory.Exists(Path.GetDirectoryName(img.Location)) == false)
        //                                    Directory.CreateDirectory(Path.GetDirectoryName(img.Location));

        //                                System.IO.File.Move(srcFileName, img.Location);
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //            return RedirectToAction("List", "Images", new { id = MoveToSubCategory.SubCategoryId });
        //        }
        //        else
        //        {
        //            int MoveToCategoryId = int.Parse(model["CategoryId"]);
        //            Category MoveToCategory = RDC.GetCategory(MoveToCategoryId);

        //            foreach (string key in model.Keys)
        //            {
        //                //get checkbox
        //                if (key.StartsWith("chkSelect_"))
        //                {
        //                    if (model[key].StartsWith("true"))
        //                    {
        //                        ImageId = int.Parse(key.Substring(10));

        //                        //move these images
        //                        Image img = RDC.GetImage(ImageId);

        //                        if (img.CategoryId != MoveToCategoryId)
        //                        {
        //                            srcFileName = img.Location;

        //                            //img.Category = MoveToCategory;

        //                            RDC.SubmitChanges();
        //                            //move the physical file
        //                            if (System.IO.File.Exists(srcFileName))
        //                            {
        //                                //create the category folder if it doesn't exist
        //                                if (Directory.Exists(Path.GetDirectoryName(img.Location)) == false)
        //                                    Directory.CreateDirectory(Path.GetDirectoryName(img.Location));

        //                                System.IO.File.Move(srcFileName, img.Location);
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //            return RedirectToAction("List", "Images", new { id = MoveToCategoryId });
        //        }
        //    }
        //    else
        //    {
        //        Image image;
        //        foreach (string key in model.Keys)
        //        {
        //            if (key.StartsWith("chkSelect_"))
        //            {
        //                if (model[key].StartsWith("true"))
        //                {
        //                    ImageId = int.Parse(key.Substring(key.IndexOf('_') + 1));
        //                    image = RDC.GetImage(ImageId);
        //                    image.Description = model["Description"];
        //                    RDC.SubmitChanges();
        //                }
        //            }
        //        }
        //        return RedirectToAction("List", "Images");
        //    }

        //}
        /// <summary>
        /// Full size image download
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Download(int id)
        {
            Image image = RDC.GetImage(id);
            if (image.FileExists)
            {
                DownloadResult result = new DownloadResult(image.Location, image.ImageDetail.UploadedFileName);
                return result;
            }
            else
            {
                return RedirectToAction("Category", "Images", new { id = image.CategoryId });
            }
        }
        public ActionResult Small(int id)
        {
            return DownloadScaledImage(id, 72, 800, 600, "small");
        }
        public ActionResult Medium(int id)
        {
            return DownloadScaledImage(id, 300, 1600, 1200, "medium");
        }
        private ActionResult DownloadScaledImage(int imageId, int dpi, int maxWidth, int maxHeight, string fileSuffix)
        {
            Image image = RDC.GetImage(imageId);
            string physicalFileName = image.Location;
            string scaledFileName = string.Empty;
            if (image.CategoryId == 1) //Imported
            {
                scaledFileName = Path.Combine(Settings.Default.ImportThumbsAndPreviewsFolder, image.ImageDetailId.ToString() + "_" + fileSuffix + ".jpg");
            }
            else
            {
                scaledFileName = Path.Combine(Settings.Default.ThumbsAndPreviewsFolder,image.ImageDetailId.ToString() + "_" + fileSuffix + ".jpg");
            }
            DownloadResult result = null;
            string downloadFileName;

            if (image.FileExists)
            {
                //resize this image...
                if (System.IO.File.Exists(scaledFileName) == false)
                {
                    if (Path.GetExtension(physicalFileName).ToLower() == ".eps")
                    {
                        Picture.SaveEpsImageAsJPGForDownload(physicalFileName, scaledFileName, dpi);
                    }
                    else
                    {
                        Picture.CreateScaledJPG(physicalFileName, scaledFileName, maxWidth, maxHeight);
                    }
                }

                //Always be a jpg file from now on for small and medium images
                downloadFileName = Path.GetFileNameWithoutExtension(image.ImageDetail.UploadedFileName) + ".jpg";

                result = new DownloadResult(scaledFileName, downloadFileName);
            }
            else
            {
                return RedirectToAction("Category", "Images", new { id = image.CategoryId });
            }

            return result;
        }
        /// <summary>
        /// Used only from import list so should not be duplicates in other categories! Watch this...
        /// </summary>
        /// <param name="id"></param>
        /// <param name="requestUrl"></param>
        /// <returns></returns>
        public ActionResult ImageDelete(int id, string requestUrl)
        {
            try
            {
                Image image = RDC.GetImage(id);
                // Check if imagedetail is in more than one category
                List<Image> images = RDC.GetImagesByImageDetail(image.ImageDetailId);
                //delete image from disk
                if (images.Count == 1)
                {
                    RDC.ImageDetails.DeleteOnSubmit(images[0].ImageDetail);
                    System.IO.File.Delete(@"C:\RedrowImageDatabase\Import\Convert\" + image.ImageDetailId + ".png");
                    System.IO.File.Delete(@"C:\RedrowImageDatabase\Import\Preview\" + image.ImageDetailId + ".png");
                    System.IO.File.Delete(@"C:\RedrowImageDatabase\Import\Imported\" + image.ImageDetail.UploadedFileName.ToString());
                }
                List<BasketImage> BasketImages = RDC.GetBasketImageByImageId(id);
                foreach (BasketImage BI in BasketImages)
                {
                    RDC.BasketImages.DeleteOnSubmit(BI);
                    RDC.SubmitChanges();
                }

                RDC.Images.DeleteOnSubmit(image);
                RDC.SubmitChanges();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { error = ex.Message + " @ " + ex.StackTrace.Substring(ex.StackTrace.LastIndexOf("\\")) });
            }

            return Redirect(requestUrl);
        }

        [Authorize]
        /// <param name="pk">ImageId</param>
        /// <returns></returns>
        public ActionResult ImageDetail(int id, int? page)
        {
            ImagesViewData viewData = new ImagesViewData();
            viewData.Image = RDC.GetImage(id);

            if (viewData.Image == null)
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Catalogs = RDC.GetCatalogs().OrderBy(p=>p.Sequence);
            // Used by the horizontal menu i.e current catalog being explored
            // viewData.Catalog = RDC.GetCatalogById(viewData.Image.Category.CatalogId);
            // Used to move policies
            ViewBag.MoveCategories = RDC.GetCategoriesByCatalogId(viewData.Image.Category.CatalogId).OrderBy(p => p.Name).ToList();
            ViewBag.MoveCatalogs = RDC.GetCatalogs().Where(p => p.CatalogId != 10).OrderBy(p => p.CatalogId).ToList();
            ViewBag.MoveSubCategories = RDC.GetSubCategoriesByCategory(viewData.Image.CategoryId).ToList();
            SubCategory subCatergory = new SubCategory();
            subCatergory.SubCategoryId = 0;
            subCatergory.CategoryId = viewData.Image.CategoryId;
            subCatergory.Name = "None";
            ViewBag.MoveSubCategories.Add(subCatergory);
            //ViewBag.Title = viewData.Image.Category.Catalog.Name + " > " + viewData.Image.Category.Name;
            // Set page number so can return to same page
            viewData.Page = page.HasValue ? page.Value : 1;
            if (viewData.Image.CategoryId == 1) // 3 = Import Catalog
                viewData.Path = Properties.Settings.Default.ImportThumbsAndPreviewsURL;
            else
                viewData.Path = Properties.Settings.Default.ThumbsAndPreviewsURL;
            return View(viewData);
        }

        [HttpPost, Authorize]
        public ActionResult ImageDetail(ImagesViewData model)
        {
            Image image = RDC.GetImage(model.Image.ImageId);
            int orignalCategoryId = image.CategoryId;
            int originalImageDetailId = image.ImageDetailId;
            string orginalUploadedFileName = image.ImageDetail.UploadedFileName;
            if (model.MoveImage)
            {
                List<Image> images = RDC.GetImagesByImageDetail(image.ImageDetailId).Where(p => p.CategoryId == model.Category.CategoryId).ToList();
                if (images.Count == 0)
                {
                    image.AdvertisingApproved = model.Image.AdvertisingApproved;
                    if (model.Image.SubCategoryId != 0 && model.Image.SubCategoryId != null)
                    {
                        image.SubCategoryId = model.Image.SubCategoryId;
                        SubCategory subCategory = RDC.GetSubCategory(model.Image.SubCategoryId.Value);
                        image.CategoryId = subCategory.CategoryId;
                    }
                    else
                        image.CategoryId = model.Category.CategoryId;
                }
                else
                {
                    RDC.Images.DeleteOnSubmit(image);
                }
                RDC.SubmitChanges();
                // Save origanal location incase it needs to be moved
                Category originalCategory = RDC.GetCategory(orignalCategoryId);
                string currentLocation = Path.Combine(originalCategory.FolderName, orginalUploadedFileName);

                Category category = RDC.GetCategory(model.Category.CategoryId);
                string newLocation = Path.Combine(category.FolderName, orginalUploadedFileName);
                if (System.IO.File.Exists(currentLocation))
                {
                    //create the category folder if it doesn't exist
                    if (Directory.Exists(Path.GetDirectoryName(newLocation)) == false)
                        Directory.CreateDirectory(Path.GetDirectoryName(newLocation));
                    // Remove if replacing an image on a new category. Remember subcats are in categories
                    if (originalCategory.CategoryId != category.CategoryId)
                    {
                        if (System.IO.File.Exists(newLocation))
                            System.IO.File.Delete(newLocation);
                        System.IO.File.Move(currentLocation, newLocation);
                    }
                }
                if (originalCategory.CategoryId == 1)
                {
                    System.IO.File.Delete(Path.Combine(Settings.Default.ThumbsAndPreviewsFolder, originalImageDetailId.ToString() + "_small.jpg"));
                    System.IO.File.Delete(Path.Combine(Settings.Default.ThumbsAndPreviewsFolder, originalImageDetailId.ToString() + "_medium.jpg"));
                    // Delete the original scaled preview and Convert images
                    System.IO.File.Delete(Path.Combine(Settings.Default.PreviewDirectory, originalImageDetailId.ToString() + ".png"));
                    System.IO.File.Delete(Path.Combine(Settings.Default.ConvertDirectory, originalImageDetailId.ToString() + ".png"));
                    // Now move the Convert from import 
                    string fromPath = Path.Combine(Settings.Default.ImportConvertDirectory, originalImageDetailId.ToString() + ".png");
                    string toPath = Path.Combine(Settings.Default.ConvertDirectory, originalImageDetailId.ToString() + ".png");
                    System.IO.File.Move(fromPath, toPath);
                    // Now move the Preview from import 
                    fromPath = Path.Combine(Settings.Default.ImportPreviewDirectory, originalImageDetailId.ToString() + ".png");
                    toPath = Path.Combine(Settings.Default.PreviewDirectory, originalImageDetailId.ToString() + ".png");
                    System.IO.File.Move(fromPath, toPath);
                }
            }
            else
            {
                if (model.CopyImage)
                {
                    Image newImage = new Image();
                    //from screen
                    newImage.CategoryId = model.Category.CategoryId;
                    if (model.Image.SubCategoryId != 0)
                    {
                        newImage.SubCategoryId = model.Image.SubCategoryId;
                    }
                    newImage.AdvertisingApproved = model.Image.AdvertisingApproved;
                    // from database
                    newImage.ImageDetailId = image.ImageDetailId;
                    RDC.Images.InsertOnSubmit(newImage);
                }
                else
                {
                    image.AdvertisingApproved = model.Image.AdvertisingApproved;
                    image.ImageDetail.Description = model.Image.ImageDetail.Description;
                    image.ImageDetail.Name = model.Image.ImageDetail.Name;
                }
                RDC.SubmitChanges();
            }
            Session["Category"] = null;
            Session["SearchText"] = null;
            Session["SearchMatchWord"] = false;
            if (image.CategoryId == 1) // 3 = Import Catalog
                model.Path = Properties.Settings.Default.ImportThumbsAndPreviewsURL;
            else
                model.Path = Properties.Settings.Default.ThumbsAndPreviewsURL;
            return RedirectToAction("List", "Images", new { catalogId = image.Category.CatalogId, page = model.Page, subCategoryId = image.SubCategoryId });
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetCategories(int id)
        {
            List<Category> categories = RDC.GetCategoriesByCatalogId(id);
            var result = (from s in categories
                          select new
                          {
                              Value = s.CategoryId,
                              Text = s.Name
                          }).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        } 
        /// <summary>
        /// Import CSV file of images and stuff
        /// </summary>
        /// 
        [Authorize]
        public ActionResult Import()
        {
            @ViewBag.Title = "Image import";
            CatalogListViewData viewData = new CatalogListViewData();
            string[] extensions = new[] { ".jpg", ".eps"};
            DirectoryInfo dinfo = new DirectoryInfo(Settings.Default.ImportDirectory);
            viewData.files =  dinfo.EnumerateFiles().Where(f => extensions.Contains(f.Extension.ToLower())).ToList();
            string contents = System.IO.File.ReadAllText(Server.MapPath("~/App_Data/") + "ImportErrors.log");
            string[] split = contents.Split(new string[] { "\r\n" }, StringSplitOptions.None);

            const int lineCount = 20;

            if (split.Length > lineCount)
                contents = string.Join("\r\n", split, split.Length - lineCount, lineCount);

            viewData.Upload = contents;

            return View(viewData);
        }
        private static IDictionary<Guid, string> tasks = new Dictionary<Guid, string>();
        [Authorize, HttpPost]
        public ActionResult Import(FormCollection model)
        {
            ImageImport ii = new ImageImport();
            ii.RootFolder = System.Web.HttpContext.Current.Server.MapPath(@"~/App_Data/");
            ii.Log = "ImportErrors.log";
            ii.DirectoryToImport = new DirectoryInfo(Properties.Settings.Default.ImportDirectory);
            HttpContext.Application["ImageImport"] = ii;

            var taskId = Guid.NewGuid();
            tasks.Add(taskId, "Processed " + ii.FilesProcessed + " of " + ii.TotalFiles + ". Added " + ii.AddedCount + " and updated " + ii.UpdatedCount + ". Working on file: " + ii.WorkingOn);

            string[] extensions = new[] { ".jpg", ".eps" };
            DirectoryInfo dinfo = new DirectoryInfo(Settings.Default.ImportDirectory);
             FileInfo[] files = dinfo.EnumerateFiles().Where(f => extensions.Contains(f.Extension.ToLower())).ToArray();
            //FileInfo[] files = new DirectoryInfo(Properties.Settings.Default.ImportDirectory).GetFiles("*.*p*", SearchOption.AllDirectories).Where(p => p.Extension != ".png").ToArray();
            ii.TotalFiles = files.Count();
            ii.LogError("");
            ii.LogError("Started import of " + files.Count() + " files");
            Task.Factory.StartNew(() =>
            {
                foreach (var file in files)
                {
                    ii.FilesProcessed++;
                    ii.WorkingOn = file.Name;
                    if (file.Name.Substring(file.Name.LastIndexOf(".")).ToLower() == ".jpg")
                    {
                        if (!ii.ImportFile(file.FullName))
                        {
                            ii.LogError(file.Name + " not uploaded");
                        }
                        else
                        {
                            ii.LogError(file.Name + " uploaded");
                            ii.AddedCount++;
                        }
                    }
                    else
                    {
                        if (!ii.ImportBlob(file.FullName))
                        {
                        ii.LogError(file.Name + " not uploaded.");
                        }
                        else
                        {
                            ii.LogError(file.Name + " uploaded");
                            ii.AddedCount++;
                        }
                    }
                    tasks[taskId] = "Processed " + ii.FilesProcessed + " of " + ii.TotalFiles + ". Added " + ii.AddedCount + " and updated " + ii.UpdatedCount + ". Working on file: " + ii.WorkingOn;
                    Thread.Sleep(2000);
                }
                tasks.Remove(taskId);
            });

            var result =  Json(taskId);
          
            return result;
        }
        public ActionResult ImportProgress(Guid id)
        {
            return Json(tasks.Keys.Contains(id) ? tasks[id] : "");
        }
        [Authorize]
        public ActionResult Integrity()
        {
            ViewBag.Categories = RDC.GetCategories().OrderBy(p => p.Name).ToList();
            return View();
        }
        [Authorize, HttpPost]
        public ActionResult Integrity(FormCollection model)
        { 
            Category category = RDC.GetCategory(int.Parse(model["Category"].ToString()));
            List<Image> Images = RDC.GetImagesByCategoryId(int.Parse(model["Category"].ToString())).ToList();
            string ErrorLog = category.Name + "_" + DateTime.Now.ToShortDateString().Replace("/", "") + ".csv";
            FileInfo diErrorOut = new FileInfo(Path.Combine(Properties.Settings.Default.ImportDirectory, ErrorLog));
            if (diErrorOut.Exists)
                diErrorOut.Delete();
            using (var line = new StreamWriter(diErrorOut.ToString()))
            {
                foreach (Image image in Images)
                {
                    if (image.ImageDetail == null)
                    {
                        line.WriteLine( "Image detail record not found for Image : " + image.ImageId.ToString() + " in " + image.ImageDetailId.ToString());
                        line.Flush();
                    }
                    else
                    {
                        string currentLocation = Path.Combine(category.FolderName, image.ImageDetail.UploadedFileName);
                        if (!System.IO.File.Exists(currentLocation))
                        {
                            line.WriteLine( "Image not found for Image : " + image.ImageId.ToString() + " in " + currentLocation.ToString());
                            line.Flush();
                        }
                    }
                }
            }
            return RedirectToAction("Integrity");
        }
    }
}
