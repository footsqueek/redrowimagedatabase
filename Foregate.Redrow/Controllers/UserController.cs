﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.Security;
using System.Web.Routing;
using System.Web;
using System.Web.Mvc;
//
using Foregate.Redrow.Models;
using Foregate.Redrow.Helpers;
using PagedList;
using System.Text;
using System.IO;
//
namespace Foregate.Redrow.Controllers
{
    [HandleError]
    [ValidateOnlyIncomingValues]
    public class UserController : Controller
    {
        RedrowDataContext RDC = new RedrowDataContext();
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }
        protected override void Initialize(RequestContext requestContext)
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }
        [Authorize(Roles = "Administrator")]
        public ActionResult Accounts()
        {
            AccountViewData viewData = new AccountViewData();
            viewData.Accounts = RDC.GetAccounts();
            return View(viewData);
        }

        protected override void Dispose(bool disposing)
        {
            this.RDC.Dispose();
            base.Dispose(disposing);
        }

        // **************************************
        // URL: /Account/Account
        // **************************************
        [Authorize(Roles = "Administrator")]
        public ActionResult Account(Guid? id)
        {
            @ViewBag.Title = "Administration Account";
            AccountViewData viewData = new AccountViewData();
            if (id.HasValue)
            {
                viewData.Account = RDC.GetAccountByUserId(id.Value);
            }
            else 
            {
                viewData.Account = new Account();
            }
            
            return View(viewData);
        }
        [HttpPost, Authorize(Roles = "Administrator")]
        public ActionResult Account(AccountViewData model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Account account = RDC.GetAccountByUserName(model.Account.UserName);
                    if (account == null)
                    {
                        MembershipCreateStatus createStatus = MembershipService.CreateUser(model.Account.UserName, model.Account.Info, model.Account.Email);
                        if (createStatus == MembershipCreateStatus.Success)
                            return RedirectToAction("Accounts", "User");
                        else
                            return RedirectToAction("Error", "Home", new { error = createStatus.ToString() });
                    }
                    else
                    {
                        account.UserName = model.Account.UserName;
                        account.Email = model.Account.Email;
                        account.Info = model.Account.Info;
                        RDC.SubmitChanges();
                    }
                    return RedirectToAction("Accounts", "User");
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Home", new { error = ex.Message + " @ " + ex.StackTrace.Substring(ex.StackTrace.LastIndexOf("\\")) });
                }
            }
            return View(model);
        }
        // **************************************
        // URL: /Account/Delete
        // **************************************
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(string id)
        {
            try
            {
                Account account = RDC.GetAccountByUserId(Guid.Parse(id));
                if (account != null)
                {
                    RDC.ClearBasket(Guid.Parse(id)) ;
                    RDC.Accounts.DeleteOnSubmit(account);
                    RDC.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { error = ex.Message + " @ " + ex.StackTrace.Substring(ex.StackTrace.LastIndexOf("\\")) });
            }
            return RedirectToAction("Accounts", "User");
        }
        [Authorize(Roles = "Administrator")]
        public ActionResult Admin()
        {
            @ViewBag.Catalogs = RDC.GetCatalogs();
            @ViewBag.Title = "Administration";
            return View();
        }
        public ActionResult ImagesToView(int value)
        {
            Account account = RDC.GetAccount();
            account.ImagesPerPage = value;
            RDC.SubmitChanges();

            return Json(new { value = value }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Manage(int id)
        {
            List<Image> images = RDC.GetImagesByCategoryId(id).ToList(); //Best streetscenes
            StringBuilder builder = new StringBuilder();
            TextWriter tw = new StreamWriter(HttpContext.Server.MapPath("~/App_Data/Error.log"),false);

            foreach (Image image in images)
            {
                if (!System.IO.File.Exists(image.Location))
                { 
                    tw.WriteLine("----------");
                    tw.WriteLine("Not at this location");
                    tw.WriteLine(image.Location);
                    string exists = Directory.GetFiles("C:\\RedrowImageDatabase\\Redrow\\", image.ImageDetail.Name + ".*", SearchOption.AllDirectories).SingleOrDefault();
                    tw.WriteLine("But found at this location");
                    tw.WriteLine(exists);
                }
            }
            tw.Close();
            return RedirectToAction("Category","Category");

        }
        // **************************************
        // URL: /Admin/Role
        // **************************************
        [Authorize(Roles = "Administrator")]
        public ActionResult Role(Guid id)
        {
            @ViewBag.Title = "Administration Role";
            AccountViewData viewData = new AccountViewData();
            Roles.RedrowRoleProvider rrp = new Roles.RedrowRoleProvider();
            viewData.Account = RDC.GetAccountByUserId(id);
            if (!viewData.Account.RoleId.HasValue)
            {
                viewData.Account.RoleId = 0;
            }
            viewData.Roles = RDC.GetRoles();
            viewData.Roles.Add(new Role() { RoleId = 0, RoleName = "None" });
            return View(viewData);
        }
        [HttpPost, Authorize(Roles = "Administrator")]
        public ActionResult Role(AccountViewData model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Account account = RDC.GetAccountByUserId(model.Account.UserUID);
                    if (model.Account.RoleId == 0) // i.e. No role
                    {
                        account.RoleId = null;
                    }
                    else
                    {
                        account.RoleId = model.Account.RoleId;
                    }
                    RDC.SubmitChanges();
                    return RedirectToAction("Accounts", "User");
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Home", new { error = ex.Message + " @ " + ex.StackTrace.Substring(ex.StackTrace.LastIndexOf("\\")) });
                }
            }
            model.Roles = RDC.GetRoles();
            return View(model);
        }
        public void SendEmail(string From, string To, string Subject, string Email)
        {
            MailMessage mailer = new MailMessage(From, To, Subject, Email);

            mailer.Priority = MailPriority.Normal;
            mailer.IsBodyHtml = true;

            SmtpClient emailClient = new SmtpClient();
            emailClient.Send(mailer);
        }
    }
    public interface IFormsAuthenticationService
    {
        void SignIn(string userName, bool createPersistentCookie);
        void SignOut();
    }

    public class FormsAuthenticationService : IFormsAuthenticationService
    {
        public void SignIn(string userName, bool createPersistentCookie)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);
        }
        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }   
    public class AccountMembershipService : IMembershipService
    {
        private readonly MembershipProvider _provider;

        public AccountMembershipService()
            : this(null)
        {
        }

        public AccountMembershipService(MembershipProvider provider)
        {
            _provider = provider ?? Membership.Provider;
        }

        public int MinPasswordLength
        {
            get
            {
                return _provider.MinRequiredPasswordLength;
            }
        }

        public bool ValidateUser(string userName, string password)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");

            return _provider.ValidateUser(userName, password);
        }

        public MembershipCreateStatus CreateUser(string userName, string password, string email)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");
            if (String.IsNullOrEmpty(email)) throw new ArgumentException("Value cannot be null or empty.", "email");

            MembershipCreateStatus status;
            _provider.CreateUser(userName, password, email, null, null, true, null, out status);
            return status;
        }

        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(oldPassword)) throw new ArgumentException("Value cannot be null or empty.", "oldPassword");
            if (String.IsNullOrEmpty(newPassword)) throw new ArgumentException("Value cannot be null or empty.", "newPassword");

            // The underlying ChangePassword() will throw an exception rather
            // than return false in certain failure scenarios.
            try
            {
                MembershipUser currentUser = _provider.GetUser(userName, true /* userIsOnline */);
                return currentUser.ChangePassword(oldPassword, newPassword);
            }
            catch (ArgumentException)
            {
                return false;
            }
            catch (MembershipPasswordException)
            {
                return false;
            }
        }
    }
    public interface IMembershipService
    {
        int MinPasswordLength { get; }
        bool ValidateUser(string userName, string password);
        MembershipCreateStatus CreateUser(string userName, string password, string email);
        bool ChangePassword(string userName, string oldPassword, string newPassword);
    }
}
