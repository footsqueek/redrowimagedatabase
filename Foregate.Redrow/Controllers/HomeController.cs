﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Net.Mail;
using System.Threading;
//
using Foregate.Redrow.Models;
using System.Net;
//
namespace Foregate.Redrow.Controllers
{
    [HandleError]
    [ValidateOnlyIncomingValues]
    public class HomeController : Controller
    {
        public HomeController()
            : this(null, null)
        {
        }
        RedrowDataContext rdc = new RedrowDataContext();
        protected override void Dispose(bool disposing)
        {
            this.rdc.Dispose();
            base.Dispose(disposing);
        }
        public HomeController(IFormsAuthentication formsAuth, MembershipProvider provider)
        {
            FormsAuth = formsAuth ?? new FormsAuthenticationWrapper();
            Provider = provider ?? Membership.Provider;
        }
        public IFormsAuthentication FormsAuth
        {
            get;
            private set;
        }
        public MembershipProvider Provider
        {
            get;
            private set;
        }
        public ActionResult About()
        {
            @ViewBag.Title = "About";

            return View();
        }
        public ActionResult Error(string error)
        {
            @ViewBag.Title = "Error";
            ViewBag.Error = error;
            return View(ViewBag);
        }
        public ActionResult Index()
        {
            @ViewBag.Title = "Home Redrow Images";
            LogOnModel model = new LogOnModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Index(LogOnModel model, bool? rememberMe)
        {
            if (ModelState.IsValid)
            {
                // Non-POST requests should just display the Login form 
                //By deafult always display the redrow catalogue i.e. id= 2

                if (User.Identity.IsAuthenticated)
                //already logged in, possible from "Remember Me?" cookie.
                {
                    return RedirectToAction("Catalogue", "Images", new { id = 2 });
                    //return RedirectToAction("Home", "Images");
                }
                else if (Request.HttpMethod != "POST")
                    return View("Index");

                // Attempt to login
                if (Provider.ValidateUser(model.UserName, model.Info))
                {
                    Account account = rdc.GetAccountByUserName(model.UserName);
                    if (account != null)
                    {
                        FormsAuth.SetAuthCookie(model.UserName, rememberMe ?? true);
                        return RedirectToAction("Catalogue", "Images", new { id = 2 });
                    }
                }
            }
            return View(model);
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            FormsAuth.SignOut();
            Session.Abandon();
            return RedirectToAction("Index");
        }
        // **************************************
        // URL: /Home/LostPassword
        // **************************************
        public ActionResult Forgotten()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Forgotten(FormCollection model)
        {
            try
            {
                Account account = rdc.GetAccountByEmailFirst(model["Email"]);
                if (account != null)
                {
                    //Send Email
                    MailMessage mailer = new MailMessage("noreply@redrow.co.uk", account.Email, "Forgotton details for Redrow Images database", "Login details \n\n Email Address: " + account.Email + "  \n\n Password: " + account.Info.Trim() + "\n \n If you have any queries or questions please contact Staunton Rook on 01244 323250");
                    SmtpClient emailClient = new SmtpClient("smtp.sendgrid.net", 587);
                    
                    emailClient.Credentials = new NetworkCredential("footsqueek", "F00tsqu33k.com");
                    

                    mailer.Priority = MailPriority.Normal;
                    mailer.IsBodyHtml = true;

                    try
                    {
                        mailer.Priority = MailPriority.Normal;
                        mailer.IsBodyHtml = false;
                        emailClient.Send(mailer);
                        ViewData["Done"] = true;
                        return View();
                    }
                    catch (SmtpFailedRecipientException ex)
                    {
                        SmtpStatusCode statusCode = ex.StatusCode;
                        if (statusCode == SmtpStatusCode.MailboxBusy ||
                            statusCode == SmtpStatusCode.MailboxUnavailable ||
                            statusCode == SmtpStatusCode.TransactionFailed)
                        {
                            // wait 5 seconds, try a second time 
                            Thread.Sleep(5000);
                            emailClient.Send(mailer);
                        }
                        else
                        {
                            Session["Error"] = ex.StatusCode;
                        }
                    }
                    finally
                    {
                        mailer.Dispose();
                       
                    }
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { error = ex.Message });
            }

            Session["errors"] = "What are you up to? If you have lost both your logon and email give us a call!";
            // If we got this far, something failed, redisplay form
            return View();
        }
        public interface IFormsAuthentication
        {
            void SetAuthCookie(string userName, bool createPersistentCookie);
            void SignOut();
            HttpCookie GetAuthCookie(string userName);
        }
        public class FormsAuthenticationWrapper : IFormsAuthentication
        {
            public void SetAuthCookie(string userName, bool createPersistentCookie)
            {
                FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);

                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(2,
                    userName,
                    DateTime.Now,
                    DateTime.Now.AddMinutes(120),
                    true,
                    "ApplicationSpecific data for this user.",
                    FormsAuthentication.FormsCookiePath);

                // Encrypt the ticket.
                string ticketEncrypted = FormsAuthentication.Encrypt(ticket);

                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, ticketEncrypted);
                cookie.HttpOnly = true;
                cookie.Path = FormsAuthentication.FormsCookiePath;
                cookie.Secure = FormsAuthentication.RequireSSL;
                cookie.Expires = ticket.Expiration;

                System.Web.HttpContext.Current.Response.Cookies.Clear();
                System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
            }

            public void SignOut()
            {
                FormsAuthentication.SignOut();
            }
            public HttpCookie GetAuthCookie(string userName)
            {
                return FormsAuthentication.GetAuthCookie(userName, true);
            }
        }
    }
}
