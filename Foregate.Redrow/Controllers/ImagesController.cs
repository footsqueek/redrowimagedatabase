﻿ using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Xml;
//
using Foregate.Redrow.Models;
using Foregate.Redrow.Helpers;
using Foregate.Helpers;
using PagedList;
using Foregate.Redrow.Properties;
//
namespace Foregate.Redrow.Controllers
{
    public class ImagesController : Controller
    {
        RedrowDataContext RDC = new RedrowDataContext();
        protected override void Dispose(bool disposing)
        {
            this.RDC.Dispose();
            base.Dispose(disposing);
        }
        [Authorize]
        public ActionResult Catalogue(int id)
        {
            Session["Category"] = null;
            Session["SearchText"] = null;
            Session["Expanded"] = string.Empty;
            Session["SearchMatchWord"] = false;

            ImagesViewData viewData = new ImagesViewData();
            viewData.Catalog = RDC.GetCatalogById(id);
            viewData.ImagesPerPage = RDC.GetAccount().ImagesPerPage;
         
       
            viewData.Images = RDC.GetImagesByCatalogId(id).GroupBy(p => p.ImageDetailId).Select(grp => grp.First()).OrderByDescending(i => i.ImageDetailId).ToPagedList(1, viewData.ImagesPerPage);
            //Used by side menu   
          
            //ViewBag.Categories = RDC.GetCategoriesByCatalogId(id).OrderBy(p => p.Name).ToList();
            //ViewBag.Portals = RDC.GetCategoriesByCatalogId(id).Where(p => p.PropertyPortal).OrderBy(p => p.Name).ToList();
            //ViewBag.Approved = RDC.GetCategoriesByCatalogId(id).Where(p => p.AdvertisingApproved).OrderBy(p => p.Name).ToList();
            ViewBag.Catalogs = RDC.GetCatalogs().OrderBy(p => p.Sequence).ToList();
            ViewBag.Categories = RDC.GetCategoriesByCatalogId(id).Where(p => !p.PropertyPortal && !p.SocialMedia && !p.AdvertisingApproved).OrderBy(p => p.Name).ToList();
            ViewBag.Portals = RDC.GetCategoriesByCatalogId(id).Where(p => p.PropertyPortal).OrderBy(p => p.Name).ToList();
            ViewBag.Social = RDC.GetCategoriesByCatalogId(id).Where(p => p.SocialMedia).OrderBy(p => p.Name).ToList();
            ViewBag.Approved = RDC.GetCategoriesByCatalogId(id).Where(p => p.AdvertisingApproved).OrderBy(p => p.Name).ToList();
            ViewBag.CatalogId = id;
            // set catagory to blank as not selected at this time
            ViewBag.SubCategories = new List<SubCategory>();
            viewData.SubCategory = new SubCategory();
            viewData.Category = new Category();
            ViewBag.Basket = RDC.GetBasket();
            ViewBag.Title = "All " + viewData.Catalog.Name;
            if (id == 3) // 3 = Import Catalog
                viewData.Path = Properties.Settings.Default.ImportThumbsAndPreviewsURL;
            else
                viewData.Path = Properties.Settings.Default.ThumbsAndPreviewsURL;
            return View("List", viewData);
        }
        [Authorize]
        public ActionResult Category(int id)
        {
            Category category = RDC.GetCategory(id);
            return RedirectToAction("List", "Images", new { categoryId = id, catalogId = category.CatalogId });
        }
        //[Authorize]
        //public ActionResult Delete(int? count)
        //{
        //    ViewBag.Count = count;
        //    return View(count);
        //}
        //[HttpPost, Authorize]
        //public ActionResult Delete(HttpPostedFileWrapper AttachmentFile)
        //{
        //    int count = 0;
        //    ExcelImport excelImport = new ExcelImport();

        //    DataTable xlsDataTable = new DataTable();
        //    xlsDataTable = excelImport.ConvertListToDataTable(AttachmentFile, Server.MapPath("~/App_Data"));
        //    foreach (DataRow dr in xlsDataTable.Rows)
        //    {
        //        try
        //        {
        //            Image image = RDC.GetImage(int.Parse(dr["ImageId"].ToString()));
        //            if (image != null)
        //            {
        //                //Move thumb and preview
        //                if (System.IO.File.Exists(Properties.Settings.Default.ThumbsAndPreviewsFolder + Path.DirectorySeparatorChar + @"Thumbs\" + image.ImageId + ".png"))
        //                {
        //                    System.IO.File.Move(Properties.Settings.Default.ThumbsAndPreviewsFolder + Path.DirectorySeparatorChar + @"Thumbs\" + image.ImageId + ".png", Properties.Settings.Default.ThumbsAndPreviewsFolder + Path.DirectorySeparatorChar + @"Delete\" + image.ImageId + "_thumb.png");
        //                }
        //                if (System.IO.File.Exists(Properties.Settings.Default.ThumbsAndPreviewsFolder + Path.DirectorySeparatorChar + @"Preview\" + image.ImageId + ".png"))
        //                {
        //                    System.IO.File.Move(Properties.Settings.Default.ThumbsAndPreviewsFolder + Path.DirectorySeparatorChar + @"Preview\" + image.ImageId + ".png", Properties.Settings.Default.ThumbsAndPreviewsFolder + Path.DirectorySeparatorChar + @"Delete\" + image.ImageId + "_preview.png");
        //                }
        //                //Now move main image
        //                if (System.IO.File.Exists(image.Location))
        //                {
        //                    System.IO.File.Move(image.Location, Properties.Settings.Default.ThumbsAndPreviewsFolder + Path.DirectorySeparatorChar + @"Delete\" + image.ImageDetail.UploadedFileName);
        //                }
        //                List<BasketImage> basketImages = RDC.GetBasketImageByImageId(image.ImageId);
        //                foreach (BasketImage bi in basketImages)
        //                {
        //                    RDC.BasketImages.DeleteOnSubmit(bi);
        //                    RDC.SubmitChanges();
        //                }
        //                RDC.Images.DeleteOnSubmit(image);
        //                RDC.SubmitChanges();
        //                count++;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            return RedirectToAction("Error", "Home", new { error = ex.Message + " @ " + ex.StackTrace.Substring(ex.StackTrace.LastIndexOf("\\")) });
        //        }
        //    }
        //    return RedirectToAction("Delete", new { count = count });
        //}
        [HttpPost, Authorize]
        public ActionResult GetImages(MoveViewData model)
        {
            if (model.FromSubCategoryId > 0)
            {
                model.Images = RDC.GetImagesBySubCat(model.FromSubCategoryId).ToList();
                SubCategory subCategory = RDC.GetSubCategory(model.FromSubCategoryId);
                model.From = subCategory.Category.Catalog.Name + " " + subCategory.Category.Name + " " + subCategory.Name;
            }
            else
            {
                model.Images = RDC.GetImagesByCategoryId(model.FromCategoryId).ToList();
                Category category = RDC.GetCategory(model.FromCategoryId);
                model.From = category.Catalog.Name + " " + category.Name;

            }
            if (model.ToSubCategoryId > 0)
            {
                SubCategory subCategory = RDC.GetSubCategory(model.ToSubCategoryId);
                model.To = subCategory.Category.Catalog.Name + " " + subCategory.Category.Name + " " + subCategory.Name; ;
            }
            else
            {
                Category category = RDC.GetCategory(model.ToCategoryId);
                model.To = category.Catalog.Name + " " + category.Name;
            }
            return View("Move", model);
        }


        public ActionResult NewSearch(string term)
        {
           
            Session["Category"] = null;

      return      RedirectToAction("List", new { categoryId = 0, catalogId = 2, term = term});

           // return List(0, null, 2, null);
        }

        /// <param name="id">Is category ID</param>
        [Authorize]
        public ActionResult List(int? categoryId, int? page, int catalogId, int? subCategoryId, string term = null)
        {

            if (term != null)
                Session["SearchText"] = term;

            ImagesViewData viewData = new ImagesViewData();
            // Set Category to empty
            viewData.Category = new Category();
            viewData.SubCategory = new SubCategory();
            viewData.ImagesPerPage = RDC.GetAccount().ImagesPerPage;
            ViewBag.SubCategories = new List<SubCategory>();

            if (Session["SearchText"] != null)
            {
                ViewBag.SearchText = Session["SearchText"].ToString();
            }

            if (categoryId != 0 && categoryId != null)
            {
                viewData.Category = RDC.GetCategory(categoryId.Value);
                if (string.IsNullOrEmpty(Session["Expanded"].ToString()) || subCategoryId.HasValue)
                    ViewBag.SubCategories = RDC.GetSubCategoriesByCategory(categoryId.Value);

                ViewBag.Title = viewData.Category.Name;
                if (subCategoryId != 0 && subCategoryId != null)
                {
                    viewData.SubCategory = RDC.GetSubCategory(subCategoryId.Value);
                    ViewBag.Title = viewData.SubCategory.Name;
                    if (Session["SearchText"] != null)
                    {
                        viewData.SearchText = Session["SearchText"].ToString();
                        if (Session["SearchMatchWord"].ToString().ToLower().Contains("true"))
                        {
                            viewData.SearchMatchWord = true;
                            viewData.Images = (
                                        from i in RDC.Images
                                        join c in RDC.SubCategories on i.SubCategoryId equals c.SubCategoryId //c.CatalogId == catalogId && 
                                        where (System.Data.Linq.SqlClient.SqlMethods.Like(i.ImageDetail.SearchDescription, "%~" + Session["SearchText"] + "~%", '¬') && c.SubCategoryId == subCategoryId)
                                        orderby i.ImageDetailId descending
                                        group i by i.ImageDetailId into s
                                        select s.First()
                                //select i
                                    ).ToPagedList(page ?? 1, viewData.ImagesPerPage);
                        }
                        else
                        {
                            viewData.SearchMatchWord = false;
                            viewData.Images = (
                                    from i in RDC.Images
                                    join c in RDC.SubCategories on i.SubCategoryId equals c.SubCategoryId //c.CatalogId == catalogId && 
                                    where ((i.ImageDetail.Name.Contains(Session["SearchText"].ToString()) || i.ImageDetail.Description.Contains(Session["SearchText"].ToString())) && c.SubCategoryId == subCategoryId)
                                    orderby i.ImageDetailId descending
                                    group i by i.ImageDetailId into s
                                    select s.First()
                                //select i
                                ).ToPagedList(page ?? 1, viewData.ImagesPerPage);
                        }
                    }
                    else
                    {
                        viewData.SearchMatchWord = false;
                        viewData.SubCategory = RDC.GetSubCategory(subCategoryId.Value);
                        ViewBag.Title = viewData.SubCategory.Name;
                        viewData.Images = RDC.GetImagesBySubCat(subCategoryId.Value).OrderByDescending(i => i.ImageDetailId).ToPagedList(page ?? 1, viewData.ImagesPerPage);
                    }
                }
                else
                {
                    if (Session["SearchText"] != null)
                    {
                        viewData.SearchText = Session["SearchText"].ToString();
                        if (Session["SearchMatchWord"].ToString().ToLower().Contains("true"))
                        {
                            viewData.SearchMatchWord = true;
                            viewData.Images = (
                                        from i in RDC.Images
                                        join c in RDC.Categories on i.CategoryId equals c.CategoryId //c.CatalogId == catalogId && 
                                        where (System.Data.Linq.SqlClient.SqlMethods.Like(i.ImageDetail.SearchDescription, "%~" + Session["SearchText"] + "~%", '¬') && !c.PropertyPortal && c.CategoryId == categoryId)
                                        orderby i.ImageDetailId descending
                                        group i by i.ImageDetailId into s
                                        select s.First()
                                //select i
                                    ).ToPagedList(page ?? 1, viewData.ImagesPerPage);
                        }
                        else
                        {
                            viewData.SearchMatchWord = false;
                            viewData.Images = (
                                    from i in RDC.Images
                                    join c in RDC.Categories on i.CategoryId equals c.CategoryId //c.CatalogId == catalogId && 
                                    where ((i.ImageDetail.Name.Contains(Session["SearchText"].ToString()) || i.ImageDetail.Description.Contains(Session["SearchText"].ToString())) && !c.PropertyPortal && c.CategoryId == categoryId)
                                    orderby i.ImageDetailId descending
                                    group i by i.ImageDetailId into s
                                    select s.First()
                                //select i
                                ).ToPagedList(page ?? 1, viewData.ImagesPerPage);
                        }
                    }
                    else
                    {
                        Session["SearchMatchWord"] = false;
                        viewData.Images = RDC.GetImagesByCategoryId(categoryId.Value).GroupBy(p => p.ImageDetailId).Select(grp => grp.First()).OrderByDescending(i => i.ImageDetailId).ToPagedList(page ?? 1, viewData.ImagesPerPage);
                    }
                }
            }
            else
            {
                if (Session["SearchText"] != null)
                {
                    viewData.SearchText = Session["SearchText"].ToString();
                    if (Session["SearchMatchWord"].ToString().ToLower().Contains("true"))
                    {
                        viewData.SearchMatchWord = false;
                        viewData.Images = (
                                    from i in RDC.Images
                                    join c in RDC.Categories on i.CategoryId equals c.CategoryId //c.CatalogId == catalogId && 
                                    where (System.Data.Linq.SqlClient.SqlMethods.Like(i.ImageDetail.SearchDescription, "%~" + Session["SearchText"] + "~%", '¬') && !c.PropertyPortal)
                                    orderby i.ImageDetailId descending
                                    group i by i.ImageDetailId into s
                                    select s.First()
                            //select i
                                ).ToPagedList(page ?? 1, viewData.ImagesPerPage);
                    }
                    else
                    {
                        viewData.SearchMatchWord = false;
                        viewData.Images = (
                                from i in RDC.Images
                                join c in RDC.Categories on i.CategoryId equals c.CategoryId //c.CatalogId == catalogId && 
                                where ((i.ImageDetail.Name.Contains(Session["SearchText"].ToString()) || i.ImageDetail.Description.Contains(Session["SearchText"].ToString())) && !c.PropertyPortal)
                                orderby i.ImageDetailId descending
                                group i by i.ImageDetailId into s
                                select s.First()
                            //select i
                            ).ToPagedList(page ?? 1, viewData.ImagesPerPage);
                    }
                }
                else
                {
                    viewData.SearchMatchWord = false;
                    viewData.Images = RDC.GetImagesByCatalogId(catalogId).GroupBy(p => p.ImageDetailId).Select(grp => grp.First()).OrderByDescending(i => i.ImageDetailId).ToPagedList(page ?? 1, viewData.ImagesPerPage);
                }
            }

            //Used by side menu
            ViewBag.Catalogs = RDC.GetCatalogs().OrderBy(p => p.Sequence).ToList();
            ViewBag.Categories = RDC.GetCategoriesByCatalogId(catalogId).Where(p => !p.PropertyPortal && !p.SocialMedia &&  !p.AdvertisingApproved).OrderBy(p => p.Name).ToList();
            ViewBag.Portals = RDC.GetCategoriesByCatalogId(catalogId).Where(p => p.PropertyPortal).OrderBy(p => p.Name).ToList();
            ViewBag.Social = RDC.GetCategoriesByCatalogId(catalogId).Where(p => p.SocialMedia).OrderBy(p => p.Name).ToList();
            ViewBag.Approved = RDC.GetCategoriesByCatalogId(catalogId).Where(p => p.AdvertisingApproved).OrderBy(p => p.Name).ToList();
            // There is always a catalog
            viewData.Catalog = RDC.GetCatalogById(catalogId);
            ViewBag.CatalogId = catalogId;
            //Used by the horizontal menu i.e current catalog being explored
            viewData.ImagesPerPage = viewData.ImagesPerPage;
            // Get basket
            ViewBag.Basket = RDC.GetBasket();
            if (catalogId == 3) // 3 = Import Catalog
            {
                viewData.Path = Properties.Settings.Default.ImportThumbsAndPreviewsURL;
                ViewBag.MoveCatalogs = RDC.GetCatalogs().Where(p => p.CatalogId != 10).OrderBy(p => p.CatalogId).ToList();
                return View("ImportList", viewData);
            }
            else
            {
                viewData.Path = Properties.Settings.Default.ThumbsAndPreviewsURL;
                Session["Expanded"] = string.Empty;
                return View(viewData);
            }
        }
        [HttpPost, Authorize]
        public ActionResult ImportList(FormCollection model)
        {
            foreach (string key in model.Keys)
            {
                if (key.StartsWith("chkSelect_"))
                {
                    if (model[key].StartsWith("true"))
                    {
                        Image image = RDC.GetImage(int.Parse(key.Substring(key.IndexOf('_') + 1)));
                        int originalCategoryId = image.CategoryId;
                        int originalImageDetailId = image.ImageDetailId;
                        int originalImageId = image.ImageId;
                        string orginalUploadedFileName = image.ImageDetail.UploadedFileName;

                        List<Image> images = RDC.GetImagesByImageDetail(image.ImageDetailId).Where(p => p.CategoryId == int.Parse(model["Category"].ToString())).ToList();
                        if (images.Count == 0)
                        {
                            image.ImageDetail.Description = model["Description"];
                            image.CategoryId = int.Parse(model["Category"].ToString());
                            image.AdvertisingApproved = model["AdvertisingApproved"].Contains("true") ? true : false;
                            if (model["SubCategory"] != null)
                            {
                                int SubCategory;
                                int.TryParse(model["SubCategory"].ToString(), out SubCategory);
                                if (SubCategory > 0)
                                {
                                    image.SubCategoryId = SubCategory;
                                }
                            }
                        }
                        else
                        {
                            RDC.Images.DeleteOnSubmit(image);
                        } 
                        RDC.SubmitChanges();
                        if (model["Alternate"].Contains("true"))
                        {
                            Image newImage = new Image();
                            //from screen
                            newImage.CategoryId = int.Parse(model["AlternateCategory"].ToString());
                            int subCategory;
                            int.TryParse(model["AlternateSubCategory"].ToString(), out subCategory);
                            if (subCategory > 0)
                            {
                                newImage.SubCategoryId = subCategory;
                            }
                            newImage.AdvertisingApproved = model["AdvertisingApproved"].Contains("true") ? true : false; ;
                            // from database
                            newImage.ImageId = originalImageId;
                            newImage.ImageDetailId = originalImageDetailId;
                            RDC.Images.InsertOnSubmit(newImage);
                        }
                        Category originalCategory = RDC.GetCategory(originalCategoryId);
                        string currentLocation = Path.Combine(originalCategory.FolderName, orginalUploadedFileName);

                        Category category = RDC.GetCategory(int.Parse(model["Category"].ToString()));
                        string newLocation = Path.Combine(category.FolderName, orginalUploadedFileName);
                        ////move the physical file
                        if (System.IO.File.Exists(currentLocation))
                        {
                            //create the category folder if it doesn't exist
                            if (Directory.Exists(Path.GetDirectoryName(newLocation)) == false)
                                Directory.CreateDirectory(Path.GetDirectoryName(newLocation));
                            // Remove if replacing an image on a new category. Remember subcats are in categories
                            if (originalCategory.CategoryId != category.CategoryId)
                            {
                                if (System.IO.File.Exists(newLocation))
                                    System.IO.File.Delete(newLocation);
                                System.IO.File.Move(currentLocation, newLocation);
                            }
                            //Now if you are importing need to move Preview and Convert. Delete existing ones 1st.
                            if (originalCategory.CategoryId == 1)
                            {
                                System.IO.File.Delete(Settings.Default.ThumbsAndPreviewsFolder + Path.DirectorySeparatorChar + originalImageDetailId.ToString() + "_small.jpg");
                                System.IO.File.Delete(Settings.Default.ThumbsAndPreviewsFolder + Path.DirectorySeparatorChar + originalImageDetailId.ToString() + "_medium.jpg");
                                System.IO.File.Delete(Settings.Default.PreviewDirectory + originalImageDetailId.ToString() + ".png");
                                System.IO.File.Delete(Settings.Default.ConvertDirectory + originalImageDetailId.ToString() + ".png");
                                // Now move the Convert and Preview
                                string fromPath = Settings.Default.ImportConvertDirectory + originalImageDetailId.ToString() + ".png";
                                string toPath = Settings.Default.ConvertDirectory + originalImageDetailId.ToString() + ".png";
                                System.IO.File.Move(fromPath, toPath);
                                fromPath = Settings.Default.ImportPreviewDirectory + originalImageDetailId.ToString() + ".png";
                                toPath = Settings.Default.PreviewDirectory + originalImageDetailId.ToString() + ".png";
                                System.IO.File.Move(fromPath, toPath);
                            }
                        }
                    }
                }
            }
            return RedirectToAction("List", "Images", new { catalogId = 3 });
        }
        [Authorize]
        public ActionResult Move(int? page)
        {
            MoveViewData viewData = new MoveViewData();
            ViewBag.Categories = RDC.GetCategories();
            ViewBag.Catalogs = RDC.GetCatalogs();
            viewData.Images = new List<Image>();
            return View(viewData);
        }
        [HttpPost, Authorize]
        public ActionResult Move(FormCollection model)
        {
            int MoveToCategoryId = int.Parse(model["ToCategoryId"]);
            Category MoveToCategory = RDC.GetCategory(MoveToCategoryId);
            int MoveToSubCategoryId  = 0;
            if (model["ToSubCategoryId"].ToString() != "0")
            {
                MoveToSubCategoryId = int.Parse(model["ToSubCategoryId"]);
            }
            string srcFileName;
            int ImageId;
           
            try
            {
                foreach (string key in model.Keys)
                {
                    //get checkbox
                    if (key.StartsWith("chkSelect_"))
                    {
                        if (model[key].StartsWith("true"))
                        {
                            ImageId = int.Parse(key.Substring(10));

                            //move these images
                            Image img = RDC.GetImage(ImageId);

                            if (img.CategoryId != MoveToCategoryId)
                            {
                                srcFileName = img.Location;
                                img.CategoryId = MoveToCategoryId;
                                RDC.SubmitChanges();
                                //move the physical file
                                if (System.IO.File.Exists(srcFileName))
                                {
                                    //create the category folder if it doesn't exist
                                    if (Directory.Exists(Path.GetDirectoryName(img.Location)) == false)
                                        Directory.CreateDirectory(Path.GetDirectoryName(img.Location));

                                    System.IO.File.Move(srcFileName, img.Location);
                                }
                            }
                            if (img.SubCategoryId != MoveToSubCategoryId && MoveToSubCategoryId != 0)
                            {
                                img.SubCategoryId = MoveToSubCategoryId;
                                RDC.SubmitChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { error = ex.Message + " @ " + ex.StackTrace.Substring(ex.StackTrace.LastIndexOf("\\")) });
            }

            return RedirectToAction("Move");
        }
        [HttpPost, Authorize]
        public ActionResult List(ImagesViewData model)
        {
            if (!string.IsNullOrEmpty( model.SearchText ))
            {
                Session["Expanded"] = string.Empty;
                Session["SearchText"] = model.SearchText;
                if (model.SearchMatchWord)
                    Session["SearchMatchWord"] = true;
                else
                    Session["SearchMatchWord"] = false;
            }
            else
            {
                Session["Expanded"] = string.Empty;
                Session["SearchText"] = null;
                Session["SearchMatchWord"] = false;
            }
            return RedirectToAction("List", "Images", new { catalogId = model.Catalog.CatalogId, categoryId = model.Category.CategoryId });
        }
        [Authorize]
        public ActionResult SubCategory(int id)
        {
            SubCategory SubCategory = RDC.GetSubCategory(id);

            return RedirectToAction("List", "Images", new { categoryId = SubCategory.CategoryId, subCategoryId = id, catalogId = SubCategory.Category.CatalogId });
        }
        [AcceptVerbs(HttpVerbs.Get), Authorize]
        public JsonResult SearchReset( )
        {
            Session["SearchText"] = null;
            Session["SearchMatchWord"] = false;
            return Json(new { data = string.Empty }, JsonRequestBehavior.AllowGet);
        }
    }
}
