﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Security;
//
using Foregate.Redrow.Models;
using Foregate.Redrow.Helpers;
using Foregate.Redrow.Properties;
using PagedList;
using Ionic.Zip;
//
namespace Foregate.Redrow.Controllers
{
    public class BasketController : Controller
    {
		RedrowDataContext RDC = new RedrowDataContext();
		 ZipFile zipfile = new ZipFile();

         protected override void Dispose(bool disposing)
         {
             this.RDC.Dispose();
             base.Dispose(disposing);
         }

         public ActionResult Index(int? page)
         {
             @ViewBag.Title = "Basket";
             int ImagesPerPage = RDC.GetAccount().ImagesPerPage;
             BasketViewData viewData = new BasketViewData();
             viewData.Basket = RDC.GetBasket();

             Guid userUID = new Guid(Membership.GetUser().ProviderUserKey.ToString());

             viewData.Images = RDC.GetBasketImages(userUID).ToPagedList(page ?? 1, ImagesPerPage);
             @ViewBag.Catalogs = RDC.GetCatalogs();
             @ViewBag.ImagesPerPage = ImagesPerPage;
             viewData.Path = Properties.Settings.Default.ThumbsAndPreviewsURL;
             return View(viewData);
         }
        [HttpPost, Authorize]
        public ActionResult Index(BasketViewData model, FormCollection collect)
        {
            Basket basket = RDC.GetBasket();
           
            foreach (BasketImage BasketImage in basket.BasketImages )
            {
                switch (collect["Size"])
                {
                    case "Small":
                        Small(BasketImage.ImageId);
                        break;
                    case "Medium":
                        Medium(BasketImage.ImageId);
                        break;
                    case "Original":
                        Image image = RDC.GetImage(BasketImage.ImageId);
                        Original(image); 
                        break;
                }
            }

            string  zipPath  = Path.Combine(Server.MapPath("~/App_Data/"), User.Identity.Name + ".zip");
            if (System.IO.File.Exists(zipPath))
            {
                //create the category folder if it doesn't exist
                System.IO.File.Delete(zipPath);
            }
            zipfile.Save(zipPath);

            DownloadResult result = new DownloadResult(zipPath, User.Identity.Name + ".zip");
            return result;
        }
        public ActionResult Add(int id, string returnUrl)
        {
            try
            {
                Basket basket = RDC.GetBasket();
                basket.AddImage(id);
                RDC.SubmitChanges();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { error = ex.Message });
            }
            return Redirect(returnUrl);
        }
        public ActionResult Remove(int id, string returnUrl)
        {
            try
            {
                Basket basket = RDC.GetBasket();
                BasketImage bi = RDC.GetImageFromBasket(basket.BasketId, id);
                RDC.BasketImages.DeleteOnSubmit(bi);
                RDC.SubmitChanges();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { error = ex.Message });
            }
            return Redirect(returnUrl); 
        }
        public ActionResult RemoveAll(string returnUrl)
        {
            try
            {
                Basket basket = RDC.GetBasket();
                RDC.BasketImages.DeleteAllOnSubmit(basket.BasketImages);
                RDC.SubmitChanges();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { error = ex.Message });
            }
            return Redirect(returnUrl);
        }
        public void Original(Image image)
        {
            zipfile.AddFile(image.Location);
        }
        public void Small(int id)
        {
            ZipScaledImage(id, 72, 800, 600, "small");
        }
        public void Medium(int id)
        {
            ZipScaledImage(id, 300, 1600, 1200, "medium");
        }
        private void ZipScaledImage(int imageId, int dpi, int maxWidth, int maxHeight, string fileSuffix)
        {
            Image image = RDC.GetImage(imageId);
            string physicalFileName = image.Location;
            string scaledFileName = Settings.Default.ThumbsAndPreviewsFolder + System.IO.Path.DirectorySeparatorChar + image.ImageId.ToString() + "_" + fileSuffix + ".jpg";

            string downloadFileName;

            if (image.FileExists)
            {
                //resize this image...
                if (System.IO.File.Exists(scaledFileName) == false)
                {
                    if (Path.GetExtension(physicalFileName).ToLower() == ".eps")
                    {
                        Picture.SaveEpsImageAsJPGForDownload(physicalFileName, scaledFileName, dpi);
                    }
                    else
                    {
                        Picture.CreateScaledJPG(physicalFileName, scaledFileName, maxWidth, maxHeight);
                    }
                }

                downloadFileName = Path.GetFileNameWithoutExtension(image.ImageDetail.UploadedFileName) + ".jpg";

                zipfile.AddFile(scaledFileName);
            }

        }
    }
}
