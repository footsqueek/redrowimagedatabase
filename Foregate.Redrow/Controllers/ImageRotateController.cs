﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace Foregate.Redrow.Controllers
{
    [Authorize]
    public class ImageRotateController : Controller
    {
        public ActionResult Rotate(int id, bool import = false)
        {
            using (var db = new Redrow.Models.RedrowDataContext())
            {
                var image = db.GetImage(id);
                if(image != null)
                {
                    int maxRetries = 5;
                    int currentRetry = 1;

                    while (currentRetry < maxRetries)
                    {
                        try
                        {
                            var path = "C:\\RedrowImages\\Preview\\" + image.ImageDetailId + ".png";
                            if (System.IO.File.Exists(path))
                            {
                                var imageObj = Image.FromFile(path);
                                var newImage =  RotateImage(imageObj, 90);
                                imageObj.Dispose();

                                System.IO.File.Delete(path);
                                newImage.Save(path);


                                //change convert image
                                var convertPath = "C:\\RedrowImages\\Convert\\" + image.ImageId + ".png";
                                var convertImage = Image.FromFile(convertPath);
                                var convertedImage = RotateImage(convertImage, 90);
                                convertImage.Dispose();

                                System.IO.File.Delete(convertPath);
                                convertedImage.Save(convertPath);

                                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { Error = "Unable to find requested image thumbnail. Path = " + path }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch 
                        {
                            currentRetry += 1;
                            System.Threading.Thread.Sleep(500);
                        }
                    }

                    return Json(new { Error = "Unable to complete rotation due to an error, please contact an administrator." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Error = "Unable to find requested image." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// method to rotate an image either clockwise or counter-clockwise
        /// </summary>
        /// <param name="img">the image to be rotated</param>
        /// <param name="rotationAngle">the angle (in degrees).
        /// NOTE: 
        /// Positive values will rotate clockwise
        /// negative values will rotate counter-clockwise
        /// </param>
        /// <returns></returns>
        public Image RotateImage(Image img, float rotationAngle)
        {
            //create an empty Bitmap image
            Bitmap bmp = new Bitmap(img.Width, img.Height);

            //turn the Bitmap into a Graphics object
            Graphics gfx = Graphics.FromImage(bmp);

            //now we set the rotation point to the center of our image
            gfx.TranslateTransform((float)bmp.Width / 2, (float)bmp.Height / 2);

            //now rotate the image
            gfx.RotateTransform(rotationAngle);

            gfx.TranslateTransform(-(float)bmp.Width / 2, -(float)bmp.Height / 2);

            //set the InterpolationMode to HighQualityBicubic so to ensure a high
            //quality image once it is transformed to the specified size
            gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;

            //now draw our new image onto the graphics object
            gfx.DrawImage(img, new Point(0, 0));

            //dispose of our Graphics object
            gfx.Dispose();

            //return the image
            return bmp;
        }
    }
}