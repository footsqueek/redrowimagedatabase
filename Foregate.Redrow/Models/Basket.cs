﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Foregate.Redrow.Models
{
	public partial class Basket
	{
		internal void AddImage(int imageId)
		{
			if (!Contains(imageId))
			{
				BasketImage bi = new BasketImage();
				bi.Basket = this;
				bi.ImageId = imageId;
				bi.CreatedDate = DateTime.Now;
				bi.UpdatedDate = DateTime.Now;

				this.BasketImages.Add(bi);
			}
		}

		internal void RemoveImage(int id)
		{
			if (Contains(id))
			{
				BasketImage bi = this.BasketImages.Where(b => b.ImageId == id).ToArray()[0];
				this.BasketImages.Remove(bi);
			}
		}

		internal bool Contains(int imageId)
		{
			return (this.BasketImages.Where(bi => bi.ImageId == imageId).SingleOrDefault() != null);
		}
	}
}
