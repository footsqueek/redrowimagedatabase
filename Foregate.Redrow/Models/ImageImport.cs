﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
//
using Foregate.Redrow.Helpers;
using Foregate.Redrow;
using System.Web.Hosting;
using Foregate.Redrow.Properties;
//
namespace Foregate.Redrow.Models
{
	public class ImageImport : IDisposable
	{
        RedrowDataContext RDC = new RedrowDataContext();

        

		public int AddedCount { get;set;}
		public int UpdatedCount { get; set; }
		public int FilesProcessed { get; set; }
		public int TotalFiles { get; set; }
		public string WorkingOn { get; set; }
        public string Log { get; set; }
		public DirectoryInfo DirectoryToImport { get; set; }
		public string DefaultDescription { get; set; }
		public string DefaultFolderName { get; set; }
		public int? CategoryId { get; set; }

		public string csvFileName
		{
			get;
			set;
		}
		public bool IsRunning
		{
			get
			{
				return (System.IO.File.Exists(FlagFile));
			}
		}
		public string RootFolder
		{
			get;
			set;
		}
		string _flagFile;
		public string FlagFile
		{
			get
			{
				if (string.IsNullOrEmpty(_flagFile) && HttpContext.Current != null)
				{
                    RootFolder = HttpContext.Current.Server.MapPath("~/App_Data/"); 
					_flagFile =  RootFolder + "ImportRunning.flg";

				}

				return _flagFile;
			}
		}


        public bool ConvertFile(string fullFileName)
        {
            try
            {
                string fileName = Path.GetFileName(fullFileName);
                if (File.Exists(fullFileName))
                {
                    Picture.ConvertImage(fullFileName, Properties.Settings.Default.ThumbsAndPreviewsFolder + Path.DirectorySeparatorChar + @"Convert\" + fileName);
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message);
                return false;
            }

            return true;
        }
		public void LogError(string msg)
		{
			try
			{
				System.IO.File.AppendAllText(Path.Combine( RootFolder , Log), ((msg != "") ? DateTime.Now.ToString() + "\t" + msg : "") + "\r\n");
			}
			catch { }
		}
        public bool ImportFile(string fullFileName)
        {
            bool imageAdd = false;
            //this is an asset row 
            try
            {
                string destinationFileName = Path.Combine(Settings.Default.ImagesFolder, "Import", "Imported", Path.GetFileName(fullFileName));
                ImageDetail newImageDetail = RDC.GetImageDetailByName(Path.GetFileNameWithoutExtension(fullFileName).ToString());
                if(File.Exists(destinationFileName))
                {
                    File.Delete(destinationFileName);
                    //checked if the File is Not already in the imported category
                    Image image = newImageDetail.Images.Where(p => p.CategoryId == 1).SingleOrDefault(); //  1 = Imported Category
                    if(image == null)
                    {
                        Image newImage = new Image();
                        newImage.CategoryId = 1; //Catalog Import and Category Imported
                        newImage.ImageDetailId = newImageDetail.ImageDetailId;
                        newImageDetail.Images.Add(newImage);
                        RDC.SubmitChanges();
                    }

                }
                else
                {
                    if (newImageDetail == null)
                    {
                        //detail
                        newImageDetail = new ImageDetail();
                        newImageDetail.UploadedFileName = Path.GetFileName(fullFileName);
                        newImageDetail.Name = Path.GetFileNameWithoutExtension(fullFileName);
                        int imageDetailId = int.Parse(newImageDetail.Name);
                        newImageDetail.ImageDetailId = imageDetailId;
                        newImageDetail.CreatedDate = DateTime.Now;
                        newImageDetail.UpdatedDate = DateTime.Now;
                        RDC.ImageDetails.InsertOnSubmit(newImageDetail);
                    }
                    //Image
                    Image image = newImageDetail.Images.Where(p => p.CategoryId == 1).SingleOrDefault(); //  1 = Imported Category
                    if (image == null)
                    {
                        Image newImage = new Image();
                        newImage.CategoryId = 1; //Catalog Import and Category Imported
                        newImage.ImageDetailId = newImageDetail.ImageDetailId;
                        newImageDetail.Images.Add(newImage);  
                        RDC.SubmitChanges();
                    }
                }
                //Removed by Nico . This all done in when moving from import section
                // delete small and medium sized download files incase the images have been enhanced
                // and eps files incase it's convert to jpg!
                //File.Delete(Settings.Default.ThumbsAndPreviewsFolder + Path.DirectorySeparatorChar  + newImageDetail.ImageDetailId.ToString() + "_small.jpg");
                //File.Delete(Settings.Default.ThumbsAndPreviewsFolder + Path.DirectorySeparatorChar  + newImageDetail.ImageDetailId.ToString() + "_medium.jpg");
                //if (File.Exists(Path.Combine(Path.GetDirectoryName(destinationFileName), Path.GetFileNameWithoutExtension(destinationFileName) + ".eps")))
                //{
                //    File.Delete(Path.Combine(Path.GetDirectoryName(destinationFileName), Path.GetFileNameWithoutExtension(destinationFileName) + ".eps"));
                //}

                Picture.SaveImage(fullFileName,
                                        newImageDetail.ImageDetailId,
                                        Path.GetFileName(destinationFileName),
                                        Path.GetDirectoryName(destinationFileName)
                                    );
                //Delete image imported
                System.IO.File.Delete(fullFileName);
                imageAdd = true;
            }
            catch (Exception ex)
            {
                LogError(ex.Message);
                return false;
            }
            return imageAdd;
        }
        public bool ImportBlob(string fullFileName)
        {
            bool wasAdded = false;
            try
            {
                string destinationFileName = Settings.Default.ImagesFolder + "\\Import\\Imported\\" + Path.GetFileName(fullFileName);
                ImageDetail newImageDetail = new ImageDetail();
                if(File.Exists(destinationFileName))
                {
                    File.Delete(destinationFileName);
                    newImageDetail = RDC.GetImageDetailByName(Path.GetFileNameWithoutExtension(fullFileName).ToString());
                    //checked if the File is Not already in the imported category
                    Image image = newImageDetail.Images.Where(p => p.CategoryId == 1).SingleOrDefault(); //  1 = Imported Category
                    if(image == null)
                    {
                        Image newImage = new Image();
                        newImage.CategoryId = 1; //Catalog Import and Category Imported
                        newImage.ImageDetailId = newImageDetail.ImageDetailId;
                        newImageDetail.Images.Add(newImage);
                        RDC.SubmitChanges();
                    }

                }
                else
                { 
                    //detail
                    newImageDetail.UploadedFileName = Path.GetFileName(fullFileName);
                    newImageDetail.Name = Path.GetFileNameWithoutExtension(fullFileName);
                    int imageDetailId = int.Parse(newImageDetail.Name);
                    newImageDetail.ImageDetailId = imageDetailId;
                    //Image
                    Image newImage = new Image();
                    newImage.CategoryId = 1; //Catalog Import and Category Imported
                    newImage.ImageDetailId = imageDetailId;
                    newImageDetail.Images.Add(newImage);
                    newImageDetail.CreatedDate = DateTime.Now;
                    newImageDetail.UpdatedDate = DateTime.Now;
                    RDC.ImageDetails.InsertOnSubmit(newImageDetail);  
                    RDC.SubmitChanges();
                }

                //automatically create the folder if it doesn't already exist
                if (Directory.Exists(Path.GetDirectoryName(destinationFileName)) == false)
                    Directory.CreateDirectory(Path.GetDirectoryName(destinationFileName));
                //Removed by Nico . This all done in when moving from import section
                // delete small and medium sized download files incase the images have been enhanced
                // and eps files incase it's convert to jpg!
                //File.Delete(Settings.Default.ThumbsAndPreviewsFolder + Path.DirectorySeparatorChar + newImageDetail.ImageDetailId.ToString() + "_small.jpg");
                //File.Delete(Settings.Default.ThumbsAndPreviewsFolder + Path.DirectorySeparatorChar + newImageDetail.ImageDetailId.ToString() + "_medium.jpg");
                //if (File.Exists(Path.Combine(Path.GetDirectoryName(destinationFileName), Path.GetFileNameWithoutExtension(destinationFileName) + ".eps")))
                //{
                //    File.Delete(Path.Combine(Path.GetDirectoryName(destinationFileName), Path.GetFileNameWithoutExtension(destinationFileName) + ".eps"));
                //}
                string pngPath = Path.Combine(Path.GetDirectoryName(fullFileName), Path.GetFileNameWithoutExtension(fullFileName) + ".png");
                if (File.Exists(pngPath))
                {
                    int PreviewSizeW = 400;
                    int PreviewSizeH = 300;
                    int ConvertSizeW = 230;
                    int ConvertSizeH = 157;
                    Picture.CreateScaledPNG(pngPath, Settings.Default.ImportPreviewDirectory + newImageDetail.ImageDetailId.ToString() + ".png", PreviewSizeW, PreviewSizeH);
                    Picture.CreateScaledPNG(pngPath, Settings.Default.ImportConvertDirectory + newImageDetail.ImageDetailId.ToString() + ".png", ConvertSizeW, ConvertSizeH);
                    //delete image object
                    System.IO.File.Delete(pngPath);
                }
                else
                {
                    string previewFileName = Path.Combine(Settings.Default.ImportPreviewDirectory , newImageDetail.ImageDetailId.ToString() + ".png");
                    string convertFileName = Path.Combine(Settings.Default.ImportConvertDirectory, newImageDetail.ImageDetailId.ToString() + ".png");
                    //Check if the file exists then delete
                    if (File.Exists(previewFileName))
                        File.Delete(previewFileName);
                    File.Copy(Path.Combine(RootFolder, "../Content/Images/camera_logo.png"), previewFileName);
                    //Check if the file exists then delete
                    if (File.Exists(convertFileName))
                        File.Delete(convertFileName);
                    File.Copy(Path.Combine(RootFolder, "../Content/Images/camera_logo.png"), convertFileName);
                }
                System.IO.File.Copy(fullFileName, destinationFileName, true);
                System.IO.File.Delete(fullFileName);
                wasAdded = true;
            }
            catch (Exception ex)
            {
                LogError(ex.Message);
            }
            return wasAdded;
        }

        public void Dispose()
        {
            this.RDC.Dispose();
        }
    }
}
