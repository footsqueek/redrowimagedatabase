﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Web.Security;
//
using Foregate.Redrow.Helpers;
using PagedList;
//
namespace Foregate.Redrow.Models
{
	//extras on image object
	public partial class Image
	{
		//get the file size of the image
		public long Size
		{
			get
			{
				long size = 0;

				if (FileExists == true)
				{
					//get the file size

					System.IO.FileInfo fileInfo = new System.IO.FileInfo(Location);

					size = fileInfo.Length;
				}

				return size;
			}
		}
		public string Location
		{
			get
			{
                if (this.CategoryId != 0)
                {
                    // This is done so we can find the orignal image path as they can be copied around
                    RedrowDataContext RDC= new RedrowDataContext();
                    if (this.CategoryId == 1)
                    {
                        Category category = RDC.GetCategory(this.CategoryId);
                        if (File.Exists(Path.Combine(category.FolderName, this.ImageDetail.UploadedFileName)))
                        {
                            RDC.Dispose();
                            return Path.Combine(category.FolderName, this.ImageDetailId.ToString() + System.IO.Path.GetExtension(this.ImageDetail.UploadedFileName));
                        }
                    }
                    else
                    {
                        List<Image> images = RDC.GetImagesByImageDetail(this.ImageDetailId);
                        foreach (Image image in images)
                        {
                            Category category = new RedrowDataContext().GetCategory(image.CategoryId);
                            if (File.Exists(Path.Combine(category.FolderName, this.ImageDetail.UploadedFileName)))
                            {
                                RDC.Dispose();
                                return Path.Combine(category.FolderName, this.ImageDetailId.ToString() + System.IO.Path.GetExtension(this.ImageDetail.UploadedFileName));
                            }
                        }
                    }
                    RDC.Dispose();
                    return string.Empty;
                }
                else
                {
                    
                    return string.Empty;
                }
			}
		}
		public bool FileExists
		{
			get 
			{
				return File.Exists(Location);
			}
		}
        public string ConvertFileName
        {
            get
            {
                return Path.Combine(Path.Combine(Properties.Settings.Default.ThumbsAndPreviewsFolder, "Convert"), this.ImageId.ToString() + ".png");
            }
        }
        public bool ConvertFileExists
        {
            get
            {
                return File.Exists(ConvertFileName);
            }
        }
		/// <summary>
		/// delete local files, and any previews
		/// </summary>
		public void DeleteFiles()
		{
			silentDelete(Location);
			silentDelete(ConvertFileName);
            //silentDelete(ThumbFileName);

			silentDelete(Path.Combine(Properties.Settings.Default.ThumbsAndPreviewsFolder, this.ImageId.ToString()) + "_small.jpg");
			silentDelete(Path.Combine(Properties.Settings.Default.ThumbsAndPreviewsFolder, this.ImageId.ToString()) + "_medium.jpg");
		}
		private void silentDelete(string fileName)
		{
			try
			{
				if (File.Exists(fileName))
					File.Delete(fileName);
			}
			catch (Exception e) 
			{
				string s = e.Message;
			}
		}
		public string FormattedSize
		{
			get
			{
				if (FileExists == false)
					return "(Missing)";
				else
					return ((decimal)((decimal)Size / 1024 / 1024)).ToString("0.0") + " MB";
			}
		}
        public bool InBasket()
        {
            RedrowDataContext RDC = new RedrowDataContext();
            Guid userUID = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            //Image image = 
            return RDC.GetBasketImage(userUID,this.ImageId); //image == null ? false : true; 
        }
        public bool SearchMatchWord { get; set; }
	}
	//extras on Category object
	public partial class Category
	{
		public string FolderName
		{
			get
			{
				return Path.Combine(this.Catalog.FolderName, this.Name.Trim());
			}
		}
       
       
	}
	//extras on Category object
	public partial class Catalog
	{
		public string FolderName
		{
			get
			{
				return Path.Combine(Properties.Settings.Default.ImagesFolder, this.Directory.Trim());
			}
		}
	}
    //extras on SubCategory object
    //public partial class SubCategory
    //{
    //    public string FolderName
    //    {
    //        get
    //        {
    //            return Path.Combine(this.Category.FolderName, this.Name.Trim());
    //        }
    //    }
    //}
    public class AccountViewData
    {
        public List<Account> Accounts { get; set; }
        public Role Role { get; set; }
        public Account Account { get; set; }
        public List<Role> Roles { get; set; }
        public bool RemoveFromRole { get; set; }
    }
    public class BasketViewData
    {
        public Basket Basket;
        public int CatalogId { get; set; }
        public int CategoryId { get; set; }
        public IPagedList<Image> Images { get; set; }
        public string Path { get; set; }
    }
    public class ImagesListViewData
    {
        public PagedList<Image> Images { get; set; }
		public Category Category { get; set; }
        public List<Catalog> Catalogs { get; set; }
        public Basket Basket;

		public ImagesListViewData()
		{ }

		public ImagesListViewData(Category category, PagedList<Image> images, Basket basket)
		{
			this.Images = images;
			this.Category = category;
			this.Basket = basket;
		}
    }
    public class ImageFull
    {
		public ImageFull(int imageId, string category, string name, string description) //string catalog, 
        {
            ImageId = imageId;
            Category = category;
            //Catalog = catalog;
            Name = name;
            Description = description;
        }

        public int ImageId { get; set; }
        public string Category { get; set; }
        public string Catalog { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

    }
    public class ImagesViewData
    {
        public IPagedList<Image> Images { get; set; }
        public Image Image { get; set; }
        public Catalog Catalog { get; set; }
        public List<ImageFull> ImageFulls { get; set; }
        public Category Category { get; set; }
        public SubCategory SubCategory { get; set; }
        public string Description { get; set; }
        public string SearchText { get; set; }
        public string Search { get; set; }
        public bool SearchMatchWord { get; set; }
        public List<Image> Basket { get; set; }
        public int ImagesPerPage { get;set; }
        public int Page { get; set; }
        public bool MoveImage { get; set; }
        public bool CopyImage { get; set; }
        public string Path { get; set; }
    }
    public class CatalogListViewData
    {
        public List<Catalog> Catalogs { get; set; }
        public Catalog Catalog { get; set; }
        public List<FileInfo> files { get; set; }
        public string Upload { get; set; }
        public int RecordCount { get; set; }
    }
    public class UserEditViewData
    {
        public Account account { get; set; }
        public List<Account> Accounts { get; set; }
        public List<Role> Roles { get; set; }
    }
    public class CategoryViewData
    {
        public List<Catalog> Catalogs { get; set; }
        public Category Category { get; set; }
        public List<Category> Categories { get; set; }
    }
    public class MoveViewData
    {
        public List<Image> Images { get; set; }
        public int FromCategoryId { get; set; }
        public int FromCatalogId { get; set; }
        public int FromSubCategoryId { get; set; }
        public int ToCategoryId { get; set; }
        public int ToCatalogId { get; set; }
        public int ToSubCategoryId { get; set; }
        public string To { get; set; }
        public string From { get; set; }

    }
}