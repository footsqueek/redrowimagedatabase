﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Foregate.Redrow.Models
{
    public partial class RedrowDataContext
    {
        public Account GetAccount()
        {
            System.Web.Security.MembershipUser user = System.Web.Security.Membership.GetUser();

            if (user != null)
            {
                return GetAccountByUserId(new Guid(user.ProviderUserKey.ToString()));
            }
            else
                return null;
            //throw new Exception("User is not logged in");
        }
        public Account GetAccountByUserId(Guid userId)
        {
            return Accounts.SingleOrDefault(p => p.UserUID == userId);
        }
        public Account GetAccountByEmailFirst(string id)
        {
            return Accounts.Where(m => m.Email == id && m.Active == true).FirstOrDefault();
        }
        public Account GetAccountByEmail(string id)
        {
            return Accounts.SingleOrDefault(p => p.Email == id);
        }
        public Account GetAccountByUserName(string id)
        {
            return Accounts.SingleOrDefault(p => p.UserName == id);
        }
        public List<Account> GetAccounts()
        {
            return Accounts.OrderByDescending(p => p.UserName).ToList();
        }
        public List<Account> GetAccountsInRole(string id)
        {
            return Accounts.Where(p => p.Role.RoleName == id).ToList();
        }
        public List<Image> GetImages()
        {
            return Images.ToList();
        }
        public Image GetImage(int id)
        {
            return Images.SingleOrDefault(p => p.ImageId == id);
        }
        public ImageDetail GetImageDetailByName(string id)
        {
            return ImageDetails.SingleOrDefault(p => p.Name == id);
        }

        public IQueryable<Image> GetImagesByCategoryId(int categoryId)
        {
            Category category = Categories.SingleOrDefault(p => p.CategoryId == categoryId);
            //if (category.AdvertisingApproved)
            //{
            //    return from i in Images
            //           join c in Categories on i.CategoryId equals c.CategoryId
            //            where c.CategoryId == categoryId
            //           && i.AdvertisingApproved
            //           group i by i.ImageDetailId into s
            //           select s.First()  ;
            //}
            //else
            //{
                return from c in Categories
                       join i in Images on c.CategoryId equals i.CategoryId
                       where c.CategoryId == categoryId
                       select i;
            //}
            //return Images.Where(p => p.CategoryId == categoryId);
        }
        public IQueryable<Image> GetImagesByCatalogId(int catalogId)
        {
            //This shit is so than can have a home button!
            if (catalogId == 10)
            {
                catalogId = 2;
            }

            return from c in Categories
                   from image in Images
                   where image.CategoryId == c.CategoryId && c.CatalogId == catalogId
                   select image;
                   

        /*  return from c in Categories
                 join ic in Images on c.CategoryId equals ic.CategoryId
                 join i in Images on ic.ImageId equals i.ImageId
                   where c.CatalogId == catalogId
                   select i;*/
            //return Images.Where(p => p.ImageCategories.CatalogId == catalogId);
        }
        public List<Image> GetImagesBySubCat(int subCategoryId)
        {
            return Images.Where(p => p.SubCategoryId == subCategoryId).ToList();
        }
        public List<Image> GetImagesByImageDetail(int id)
        {
           return Images.Where(p => p.ImageDetailId == id).ToList();
        }
        public List<Category> GetCategories()
        {
            return Categories.ToList();
        }
        public List<Category> GetCategoriesByCatalogId(int catalogId)
        {
            //This shit is so than can have a home button!
            if (catalogId == 10)
            {
                catalogId = 2;
            }
            return Categories.Where(p => p.CatalogId == catalogId).ToList();
        }
        public List<Category> GetCategoriesByCatalogName(string catalogName)
        {
            //This shit is so than can have a home button!
            if (catalogName == "Home")
            {
                catalogName = "Images";
            }
            return Categories.Where(p => p.Catalog.Name == catalogName).ToList();
        }
        public List<Catalog> GetCatalogs()
        {
            Account account = GetAccount();
            string roleName = string.Empty;
            if (account.Role != null)
            {
                roleName = account.Role.RoleName;
            }
            switch  (roleName)
            {
                case "PowerUser":
                    return Catalogs.Where(c => c.AllUsers || c.Name == "Archive").OrderBy(p => p.Sequence).ToList();
                case "Administrator":
                    return Catalogs.OrderBy(p => p.Sequence).ToList();
                default:
                    return Catalogs.Where(c => c.AllUsers).OrderBy(p => p.Sequence).ToList();
            }
            
        }
        public Catalog GetCatalogById(int id)
        {
            return Catalogs.SingleOrDefault(p => p.CatalogId == id);
        }
        public Catalog GetCatalogByName(string catalog)
        {
            return Catalogs.SingleOrDefault(p => p.Name == catalog);
        }
        public Category GetCategory(int id)
        {
            return Categories.SingleOrDefault(p => p.CategoryId == id);
        }
        public List<ImageFull> GetImageFulls(int categoryId)
        {
            return (from  i in Images
                    where i.CategoryId == categoryId
                    select new ImageFull(i.ImageId, i.Category.Name, i.ImageDetail.Name, i.ImageDetail.Description)).ToList(); //i.Catalog.Name, 
        }
        public Basket GetBasket()
        {
            return GetBasket(true);
        }
        // This isued to remove images that are about to be delete
        // from all users baskets.
        public List<BasketImage> GetBasketImageByImageId(int id)
        {
            return BasketImages.Where(p=>p.ImageId == id).ToList();
        }
        public Basket GetBasket(bool createIfNone)
        {
            Guid userUID = new Guid(Membership.GetUser().ProviderUserKey.ToString());

            Basket basket = (this.Baskets.Where(b => b.UserId == userUID).SingleOrDefault());

            if (basket == null && createIfNone)
            {
                basket = new Basket();
                basket.UserId = userUID;
                basket.CreatedDate = DateTime.Now;
                basket.UpdatedDate = DateTime.Now;

                this.Baskets.InsertOnSubmit(basket);
                this.SubmitChanges();
            }

            return basket;
        }
        //remove a user's basket
        public void ClearBasket()
        {
            Guid userUID = new Guid(Membership.GetUser().ProviderUserKey.ToString());

            ClearBasket(userUID);
        }
        public void ClearBasket(Guid userUID)
        {
            Basket basket = (this.Baskets.Where(b => b.UserId == userUID).SingleOrDefault());

            if (basket != null)
            {
                this.Baskets.DeleteOnSubmit(basket);
                this.SubmitChanges();
            }
        }
        public IQueryable<Image> GetBasketImages(Guid id)
        {
            return from i in Images
                         join bi in BasketImages on i.ImageId equals bi.ImageId
                         join b2 in Baskets on bi.BasketId equals b2.BasketId
                         where b2.UserId == id
                         orderby bi.CreatedDate
                         select i;
        }
        public BasketImage GetImageFromBasket(int basketId, int imageId)
        {
            return BasketImages.SingleOrDefault(p => p.BasketId == basketId && p.ImageId == imageId);
        }
        public bool GetBasketImage(Guid id, int imageId)
        {
            return Baskets.SingleOrDefault(p => p.UserId == id).BasketImages.Where(p => p.ImageId == imageId).Count() > 0;
        }
        public Role GetRole(int id)
        {
            return Roles.SingleOrDefault(p => p.RoleId == id);
        }
        public Role GetRoleByRoleName(string id)
        {
            return Roles.SingleOrDefault(p => p.RoleName == id);
        }
        public List<Role> GetRoles()
        {
            return Roles.ToList();
        }
        public SubCategory GetSubCategory(int id)
        {
            return SubCategories.SingleOrDefault(p => p.SubCategoryId == id);
        }
        public List<SubCategory> GetSubCategories()
        {
            return SubCategories.ToList();
        }
        public List<SubCategory> GetSubCategoriesByCategory(int id)
        {
            return SubCategories.Where(p => p.CategoryId == id).ToList();
        }
    }
}