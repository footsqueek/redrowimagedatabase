﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Foregate.Redrow.Helpers
{
    public static class Button
    {
        public static string ActionButton(this HtmlHelper helper, string text, string url,string css)
        {
            return "<input type=\"button\" class=\"" + css + "\" value=\"" + text + "\" onclick=\"document.location.href = '" + url + "'\" />";
        }
        public static string SubmitButton(this HtmlHelper helper, string text)
        {
            return "<input type=\"submit\" class=\"buttonNews\" value=\"" + text + "\" />";
        }
        public static string SubmitLongButton(this HtmlHelper helper, string text)
        {
            return "<input type=\"submit\" class=\"longbuttonNews\" value=\"" + text + "\" />";
        }
        public static MvcHtmlString ActionButton3(this HtmlHelper helper, string text, string url, string css)
        {
            return MvcHtmlString.Create( "<button type=\"button\" class=\"" + css + "\" id=\"" + text + "\" onclick=\"document.location.href = '" + url + "'\" >" + text+ "</button>");
        }
        public static MvcHtmlString SubmitButton3(this HtmlHelper helper, string text)
        {
            return MvcHtmlString.Create( "<button type=\"submit\" class=\"buttonNews\" id=\"" + text + "\" >" + text + "</button>");
        }
        public static MvcHtmlString SubmitLongButton3(this HtmlHelper helper, string text)
        {
            return MvcHtmlString.Create( "<button type=\"submit\" class=\"longbuttonNews\" id=\"" + text + "\" >" + text + "</button>");
        }
    }
}
