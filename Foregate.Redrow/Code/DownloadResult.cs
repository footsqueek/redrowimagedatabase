﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Mvc;
//
using Foregate.Redrow.Models;
//
namespace Foregate.Redrow
{
    public class DownloadResult : ActionResult
    {
        public string PhysicalFilePath { get; set; }
        //public string FileDownloadName { get; set; }

        private string UploadedFileName { get; set; }

        public DownloadResult(string physicalFilePath, string uploadedFileName)
        {
            this.PhysicalFilePath = physicalFilePath;
            this.UploadedFileName = uploadedFileName;
        }

        public DownloadResult(Foregate.Redrow.Models.Image image)
        {
            this.PhysicalFilePath = image.Location;
            this.UploadedFileName = image.ImageDetail.UploadedFileName;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (!String.IsNullOrEmpty(this.UploadedFileName))
            {
                switch (System.IO.Path.GetExtension(this.UploadedFileName.ToLower()))
                {
                    case ".eps":
                        context.HttpContext.Response.AddHeader("Content-Type", "application/octet-stream");
                        break;
                    case ".wmv":
                        context.HttpContext.Response.AddHeader("Content-Type", "application/octet-stream");
                        break;
                    default:
                        context.HttpContext.Response.AddHeader("Content-Type", "image/" + System.IO.Path.GetExtension(this.UploadedFileName).Substring(1));
                        break;

                }
                //context.HttpContext.Response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", this.UploadedFileName));
                //context.HttpContext.Response.TransmitFile(this.PhysicalFilePath);
                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.ClearContent();
                response.Clear();
                response.ContentType = "text/plain";
                response.AddHeader("Content-Disposition", "attachment; filename=" + this.UploadedFileName);
                response.TransmitFile(this.PhysicalFilePath);                //  Transmit File
                response.Flush();            
                response.End();
            }
        }
    }
}
