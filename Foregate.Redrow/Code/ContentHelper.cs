﻿using System;
using System.Web.Mvc;
using System.Web;

namespace Foregate.Redrow
{
    public static class ContentHelper
    {
        public static string ContentRoot
        {
            get { return "/Content"; }
        }

        public static string DBImagesRoot
        {
            get { return String.Format("{0}/db", ImagesRoot); }
        }

        public static string ImagesRoot
        {
            get { return String.Format("{0}/Images", ContentRoot); }
        }

        public static string StyleRoot
        {
            get { return String.Format("{0}/css", ContentRoot); }
        }

        public static string JavaScriptRoot
        {
            get { return String.Format("{0}/js", ContentRoot); }
        }
        public static string FlashRoot
        {
            get { return String.Format("{0}/Flash", ContentRoot); }
        }
    }

    public static class UrlHelperExtensions
    {
        public static string ContentArea()
        {
            HttpRequest httpRequest = HttpContext.Current.Request;
            if (httpRequest.Browser.IsMobileDevice)
            {
                return "../../../..";
            }
            else
            {
                return "../../..";
            }
        }
    }
}
