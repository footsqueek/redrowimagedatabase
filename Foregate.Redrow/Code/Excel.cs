﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Foregate.Helpers
{
    public class ExcelImport
    {
        public DataTable ConvertListToDataTable(HttpPostedFileWrapper AttachmentFile, string paths)
        {
            if (AttachmentFile.ContentLength > 0)
            {
                var fileName = Path.GetFileName(AttachmentFile.FileName);
                var path = Path.Combine(paths, fileName);
                AttachmentFile.SaveAs(path);
                DataTable table = ProduceDataTable(path);
                System.IO.File.Delete(path);
                return table;
            }
            else
            {
                return null;
            }
        }
        public DataTable ProduceDataTable(string path)
        {
            List<string[]> parsedData = new List<string[]>();
            DataTable table = new DataTable();
            try
            {
                using (StreamReader readFile = new StreamReader(path))
                {
                    string line;
                    string[] row;

                    while ((line = readFile.ReadLine()) != null)
                    {
                        row = line.Split(',');
                        parsedData.Add(row);
                    }
                }


                int columns = 0;
                foreach (var array in parsedData)
                {
                    if (array.Length > columns)
                    {
                        columns = array.Length;
                    }
                }

                string[] header = parsedData[0];
                parsedData.Remove(header);
                // Add columns.
                for (int i = 0; i < columns; i++)
                {
                    table.Columns.Add(header[i]);
                }

                // Add rows.
                foreach (var array in parsedData)
                {
                    table.Rows.Add(array);
                }
            }
            catch
            {
                return null;
            }
            return table;
        }
    }
}