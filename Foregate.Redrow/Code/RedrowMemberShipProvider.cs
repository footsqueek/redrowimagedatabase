﻿using System.Web.Security;
using System.Configuration.Provider;
using System.Collections.Specialized;
using System;
using System.Data;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using System.Web;
using System.Globalization;
//
using Foregate.Redrow.Models;
using Foregate.Helpers;
//
namespace Foregate.Redrow.Roles
{
    public class RedrowMembershipProvider : MembershipProvider
    {
        RedrowDataContext RDC = new RedrowDataContext();
        Encryptor encryptor = new Encryptor();
        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            if (String.IsNullOrEmpty(username)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(oldPassword)) throw new ArgumentException("Value cannot be null or empty.", "oldPassword");
            if (String.IsNullOrEmpty(newPassword)) throw new ArgumentException("Value cannot be null or empty.", "newPassword");

            // The underlying ChangePassword() will throw an exception rather
            // than return false in certain failure scenarios.
            try
            {
                Account currentUser = RDC.GetAccountByUserName(username);
                if (currentUser.Info == oldPassword)
                {
                    currentUser.Info = newPassword;
                    RDC.SubmitChanges();
                    return true;
                }
                else
                    return false;
            }
            catch (ArgumentException)
            {
                return false;
            }
            catch (MembershipPasswordException)
            {
                return false;
            }
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            try
            {
                Account account = RDC.GetAccountByUserName(username);
                if (account == null)
                {
                    account = new Account();
                    account.UserName = username;
                    account.Info = password;
                    account.Email = email;
                    account.UserUID = Guid.NewGuid();
                    account.Active = true;
                    account.CreatedDate = DateTime.Now;
                    account.UpdatedDate = DateTime.Now;
                    account.CreatedBy = RDC.GetAccount().UserUID;
                    account.ImagesPerPage = 16;
                    RDC.Accounts.InsertOnSubmit(account);
                    RDC.SubmitChanges();
                    status = MembershipCreateStatus.Success;
                }
                else
                {
                    status = MembershipCreateStatus.DuplicateUserName;
                }
            }
            catch
            {
                status = MembershipCreateStatus.UserRejected;
            }
           
            return null;
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException("GetAllUsers");
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            Account runner = RDC.GetAccountByUserName(username);

            if (runner == null)
            {
                Account account = RDC.GetAccountByUserName(username);
                if (account == null)
                    return null;
                else
                    return new MembershipUser(
                                            this.Name,
                                            account.UserName,
                                            account.UserUID,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            true,
                                            false,
                                            account.CreatedDate,
                                            DateTime.Now,
                                            DateTime.Now,
                                            DateTime.Now,
                                            DateTime.Now
                                            );
            }
            else
                return new MembershipUser(
                                            this.Name,
                                            runner.UserName,
                                            runner.UserUID,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            true,
                                            false,
                                            runner.CreatedDate,
                                            DateTime.Now,
                                            DateTime.Now,
                                            DateTime.Now,
                                            DateTime.Now
                                            );

        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string username, string password)
        {
            bool answer = false;
            Account account = new Account();

            account = RDC.GetAccountByUserName(username);

            if (account != null)
            {
                if (account.Info == password)
                {
                    answer = true;
                }
            }
            return answer;
        }

        public override string ApplicationName
        {
            get
            {
                //throw new NotImplementedException();

                return "Foregate.Redrow";
            }
            set
            {
                //throw new NotImplementedException();
            }
        }
    }
}
