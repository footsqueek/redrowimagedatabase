﻿
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System;
using System.Text.RegularExpressions;

namespace Foregate.Helpers
{
    public class Encryptor
    {
        const string DESKey = "REDROW123456789#";
        const string DESIV = "WORDER123456789#";
        public string EncryptString(string ClearText, string DESKey, string DESIV)
        {
            byte[] clearTextBytes = Encoding.UTF8.GetBytes(ClearText);

            System.Security.Cryptography.SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();
            rijn.Padding = PaddingMode.ANSIX923;

            MemoryStream ms = new MemoryStream();
            byte[] IV = Encoding.ASCII.GetBytes(DESIV);
            byte[] key = Encoding.ASCII.GetBytes(DESKey);
            CryptoStream cs = new CryptoStream(ms, rijn.CreateEncryptor(key, IV),
                                                    CryptoStreamMode.Write);

            cs.Write(clearTextBytes, 0, clearTextBytes.Length);

            cs.Close();

            return ByteArrToString(ms.ToArray());// Convert.ToBase64String(ms.ToArray());
        }
        public string EncryptString(string ClearText)
        {
            byte[] clearTextBytes = Encoding.UTF8.GetBytes(ClearText);

            System.Security.Cryptography.SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();
            rijn.Padding = PaddingMode.ANSIX923;

            MemoryStream ms = new MemoryStream();
            byte[]IV = Encoding.ASCII.GetBytes(DESIV);
            byte[]key = Encoding.ASCII.GetBytes(DESKey);
            CryptoStream cs = new CryptoStream(ms, rijn.CreateEncryptor(key, IV),
                                                    CryptoStreamMode.Write);

            cs.Write(clearTextBytes, 0, clearTextBytes.Length);

            cs.Close();

            return ByteArrToString(ms.ToArray());// Convert.ToBase64String(ms.ToArray());
        }
        public string ByteArrToString(byte[] byteArr)
        {
            byte val; 
            string tempStr = "";
            for (int i = 0; i <= byteArr.GetUpperBound(0); i++)
            {
                val = byteArr[i];
                if (val < (byte)10) tempStr += "00" + val.ToString();
                else if (val < (byte)100) tempStr += "0" + val.ToString();
                else tempStr += val.ToString();
            }
            return tempStr;
        }
        public byte[] StrToByteArray(string str)
        {
            byte[] byteArr = null;
            if (str.Length != 0)
            {
                byte val;
                byteArr = new byte[str.Length / 3];
                int i = 0;
                int j = 0;
                do
                {
                    val = byte.Parse(str.Substring(i, 3));
                    byteArr[j++] = val; i += 3;
                }
                while (i < str.Length);
            }
            return byteArr;
        }
        public string DecryptString(string EncryptedText, string DESKey, string DESIV)
        {
            byte[] encryptedTextBytes = StrToByteArray(EncryptedText); // Convert.FromBase64String(EncryptedText);

            MemoryStream ms = new MemoryStream();

            System.Security.Cryptography.SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();
            rijn.Padding = PaddingMode.ANSIX923;

            byte[] rgbIV = Encoding.ASCII.GetBytes(DESIV);
            byte[] key = Encoding.ASCII.GetBytes(DESKey);

            CryptoStream cs = new CryptoStream(ms, rijn.CreateDecryptor(key, rgbIV),
                                                CryptoStreamMode.Write);

            cs.Write(encryptedTextBytes, 0, encryptedTextBytes.Length);

            cs.Close();

            return Encoding.UTF8.GetString(ms.ToArray());

        }
        public string DecryptString(string EncryptedText)
        {
            byte[] encryptedTextBytes = StrToByteArray(EncryptedText); // Convert.FromBase64String(EncryptedText);

            MemoryStream ms = new MemoryStream();

            System.Security.Cryptography.SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();
            rijn.Padding = PaddingMode.ANSIX923;

            byte[] rgbIV = Encoding.ASCII.GetBytes(DESIV);
            byte[] key = Encoding.ASCII.GetBytes(DESKey);

            CryptoStream cs = new CryptoStream(ms, rijn.CreateDecryptor(key, rgbIV),
                                                CryptoStreamMode.Write);

            cs.Write(encryptedTextBytes, 0, encryptedTextBytes.Length);

            cs.Close();

            return Encoding.UTF8.GetString(ms.ToArray());

        }
        public bool IsNumeric(string strValue)
        {
            Regex objNotWholePattern = new Regex("[^0-9]");
            return !objNotWholePattern.IsMatch(strValue)
                 && (strValue != "");
        }
    }
}