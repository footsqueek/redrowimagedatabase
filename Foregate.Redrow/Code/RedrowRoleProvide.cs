﻿using System.Web.Security;
using System.Configuration.Provider;
using System.Collections.Specialized;
using System;
using System.Data;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using System.Web;
using System.Globalization;
using System.Data.SqlClient;  
using System.Security.Cryptography;   
using System.Text;    
using System.Web.Configuration; 
//
using Foregate.Redrow.Models;
using Foregate.Helpers;
//
namespace Foregate.Redrow.Roles
{

    public sealed class RedrowRoleProvider : RoleProvider
    {
        RedrowDataContext WDC = new RedrowDataContext();
        private string applicationName;
        public override string ApplicationName
        {
            get
            {
                return applicationName;
            }
            set
            {
                applicationName = value;
            }
        }
        /// <summary>     
        /// Initialize.        
        /// </summary>       
        /// <param name="usernames"></param>      
        /// <param name="roleNames"></param>       
        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
            { throw new ArgumentNullException("config"); }
            if (name == null || name.Length == 0)
            {
                name = "CustomRoleProvider";
            }
            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Custom Role Provider");
            }
            //Initialize the abstract base class.             
            base.Initialize(name, config);
            applicationName = GetConfigValue(config["applicationName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
        }
        /// <summary>       
        /// Add users to roles.      
        /// </summary>      
        /// <param name="usernames"></param>   
        /// <param name="roleNames"></param>     
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            try
            {
                foreach (string username in usernames)
                {
                    // find each user in users table        
                    Account user = WDC.GetAccountByUserName(username);
                    if (user != null)
                    {

                        // find all roles that are contained in the roleNames              
                        var AllDbRoles = WDC.GetRoles() ;
                        List<int> UserRoles = new List<int>();
                        foreach (var role in AllDbRoles)
                        {
                            foreach (string roleName in roleNames)
                            {
                                if (role.RoleName == roleName)
                                {
                                    UserRoles.Add(role.RoleId);
                                    continue;
                                }
                            }
                        }
                        if (UserRoles.Count > 0)
                        {
                            foreach (var roleId in UserRoles)
                            {
                                Account UIR = WDC.GetAccountByUserId(user.UserUID);
                                if (UIR != null)
                                {
                                    user.RoleId = roleId;
                                }
                                WDC.SubmitChanges();
                            }
                        }
                    }
                }
            }
            catch
            { }
        }
        /// <summary>         

        /// Create new role.        
        /// /// </summary>        
        /// /// <param name="roleName"></param>    
        public override void CreateRole(string roleName)
        {
            try
            {
                Role role = new Role();
                role.RoleName = roleName;
                WDC.Roles.InsertOnSubmit(role);
                WDC.SubmitChanges();
            }
            catch
            { }
        }
        /// <summary>  
        /// Delete role.     
        /// </summary>    
        /// <param name="roleName"></param>    
        /// <param name="throwOnPopulatedRole"></param>       
        /// <returns>true if role is successfully deleted</returns>    
        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            bool ret = false;
                try
                {
                    Role role = WDC.GetRoleByRoleName(roleName);
                    if (role != null)
                    {
                        WDC.Roles.DeleteOnSubmit(role);
                        WDC.SubmitChanges();
                        ret = true;
                    }
                }
                catch
                {
                    ret = false;
                }
            return ret;
        }
        /// <summary>      
        /// Find users in role.  
        /// </summary>       
        /// <param name="roleName"></param> 
        /// <param name="usernameToMatch"></param>     
        /// <returns></returns>    
        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            List<string> users = new List<string>();
            try
            {
                List<Account> usersInRole = WDC.GetAccountsInRole(roleName);
                if (usersInRole != null)
                {
                    foreach (Account account in usersInRole)
                    {
                        if (account.UserName == usernameToMatch)
                        {
                            users.Add(account.UserName);
                        }
                    }
                }
            }
            catch
            { }
            return users.ToArray();
        }
        /// <summary>       
        /// /// Get all roles.  
        /// /// </summary>     
        /// /// <returns></returns> 
        public override string[] GetAllRoles()
        {
            List<string> roles = new List<string>();
            try
            {
                var dbRoles = WDC.Roles;
                foreach (var role in dbRoles)
                {
                    roles.Add(role.RoleName);
                }
            }
            catch
            { }
            return roles.ToArray();
        }
        /// <summary>    
        /// Get all roles for a specific user.   
        /// </summary>     
        /// <param name="username"></param>   
        /// <returns></returns>      
        public override string[] GetRolesForUser(string username)
        {
            List<string> roles = new List<string>();
            try
            {
                Account dbRoles = WDC.GetAccountByUserName(username);
                if (dbRoles != null)
                    roles.Add(dbRoles.Role.RoleName);
                else
                {
                    Account account = WDC.GetAccountByUserName(username);
                    if (account != null)
                        roles.Add(account.Role.RoleName);
                }
            }
            catch { }

            return roles.ToArray();
        }
        /// <summary>     
        /// Get all users that belong to a role.       
        /// </summary>        
        /// <param name="roleName"></param>  
        /// <returns></returns>      
        public override string[] GetUsersInRole(string roleName)
        {
            List<string> users = new List<string>();

            try
            {
                var usersInRole = WDC.GetAccountsInRole(roleName);
                if (usersInRole != null)
                {
                    foreach (var userInRole in usersInRole)
                    {
                        users.Add(userInRole.UserName);
                    }
                }
            }
            catch { }

            return users.ToArray();
        }
        /// <summary>    
        /// Checks if user belongs to a given role.     
        /// </summary>   
        /// <param name="username"></param>   
        /// <param name="roleName"></param>      
        /// <returns></returns>    
        public override bool IsUserInRole(string username, string roleName)
        {
            bool isValid = false;
            try
            {
                var usersInRole = WDC.GetAccountsInRole(roleName);
                if (usersInRole == null)
                {
                    var accountsInRole = WDC.GetAccountsInRole(roleName);
                    foreach (Account account in accountsInRole)
                    {
                        if (account.UserName == username)
                        {
                            isValid = true;
                        }
                    }
                }
                else
                {
                    foreach (Account account in usersInRole)
                    {
                        if (account.UserName == username)
                        {
                            isValid = true;
                        }
                    }
                }
            }
            catch
            {
                isValid = false;
            }
            return isValid;
        }
        /// <summary>      
        ///      
        /// </summary>    
        /// <param name="usernames"></param>       
        /// <param name="roleNames"></param>     
        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            try
            {
                foreach (string username in usernames)
                {
                    // find each user in users table         
                   Account user = WDC.GetAccountByUserName(username);
                    if (user != null)
                    {
                        // find all roles that are contained in the roleNames                
                        var AllDbRoles = WDC.Roles;
                        List<int> RemoveRoleIds = new List<int>();
                        foreach (var role in AllDbRoles)
                        {
                            foreach (string roleName in roleNames)
                            {
                                if (role.RoleName == roleName)
                                {
                                    RemoveRoleIds.Add(role.RoleId);
                                    continue;
                                }
                            }
                        }
                        if (RemoveRoleIds.Count > 0)
                        {
                            foreach (var roleId in RemoveRoleIds)
                            {
                                Account UIR = WDC.GetAccountByUserId(user.UserUID);
                                if (UIR != null)
                                {
                                    if (UIR.RoleId == roleId)
                                    {
                                        UIR.RoleId = int.MinValue;
                                        WDC.SubmitChanges();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch
            { }
        }
        /// <summary>      
        /// Check if role exists.  
        /// </summary>      
        /// <param name="configValue"></param>    
        /// <param name="defaultValue"></param>       
        /// <returns></returns>       

        public override bool RoleExists(string roleName)
        {
            bool isValid = false;

            // check if role exits            
            if (WDC.GetRoleByRoleName(roleName) != null)
            {
                isValid = true;
            }
            return isValid;
        }
        /// <summary>        
        /// Get config value.    
        /// </summary>        
        /// <param name="configValue"></param>      
        /// <param name="defaultValue"></param>     
        /// <returns></returns>        
        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
            {
                return defaultValue;
            }
            return configValue;
        }
    }
}
