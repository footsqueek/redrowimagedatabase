﻿using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.UI;
using System.Web.Routing;
//
using GhostscriptSharp;
using WebSupergoo.ImageGlue6;
using Foregate.Redrow.Models;
using Foregate.Redrow.Properties;

namespace Foregate.Redrow.Helpers
{
    public class Picture
    {
		public const int ConvertSizeW = 230;
		public const int ConvertSizeH = 157;
        public const int PreviewSizeW = 400;
        public const int PreviewSizeH = 300;

		/// <summary>
		/// Save an uploaded image to disk, and create thumnail and preview images
		/// </summary>
		/// <param name="imageStream"></param>
		/// <param name="fileName"></param>
		/// <param name="productImagesPath"></param>
		/// <returns></returns>
        public static void SaveImage(
                string fullFileName,
                int imageId,
                string fileName,
                string imagesPath)
        {

            string imageName = imageId.ToString();

            //Thumb no longer used
            CreateScaledPNG(fullFileName, @Settings.Default.ImportPreviewDirectory + imageName + ".png", PreviewSizeW, PreviewSizeH);
//@"C:\RedrowImageDatabase\Import\Preview\"
            CreateScaledPNG(fullFileName, @Settings.Default.ImportConvertDirectory + imageName + ".png", ConvertSizeW, ConvertSizeH);
//@"C:\RedrowImageDatabase\Import\Convert\" + imageName + ".png"
            //automatically create the folder if it doesn't already exist
            if (Directory.Exists(imagesPath) == false)
                Directory.CreateDirectory(imagesPath);

            //Check if the file exists as .eps then delete
            string fullPath = Path.Combine(imagesPath, imageName + ".eps");
            if (File.Exists(fullPath))
                File.Delete(fullPath);
            //Now Check if the file exists as current extension then delete
            fullPath = Path.Combine(imagesPath, imageName + Path.GetExtension(fileName));
            if (File.Exists(fullPath))
                File.Delete(fullPath);
            // Delete old medium or small images
            System.IO.File.Delete(Path.Combine(Settings.Default.ImportThumbsAndPreviewsFolder, imageName.ToString() + "_small.jpg"));
            System.IO.File.Delete(Path.Combine(Settings.Default.ImportThumbsAndPreviewsFolder, imageName.ToString() + "_medium.jpg"));
            //Move original file to new location
            File.Move(fullFileName, fullPath);
        }
        public static void SaveEpsImageAsJPGForDownload(string epsFileName, string outputFileName, int resolution)
        {
            using (Canvas canvas = new Canvas())
            {
                canvas.DrawFile(epsFileName, "");
                GhostscriptWrapper.GeneratePageThumb(epsFileName, outputFileName, 1, canvas.Width, canvas.Height, resolution);
            }
        }
        public static void CreateScaledJPG(string fileName, string outputFileName, int maxWidth, int maxHeight)
        {
            System.Drawing.Image FullsizeImage = System.Drawing.Image.FromFile(fileName);

            // Prevent using images internal thumbnail
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

            float nPercentWidth =  ((float)maxWidth / FullsizeImage.Width);
            float nPercentHeight = ((float)maxHeight / FullsizeImage.Height);

            //use smallest percent
            float percentScale = (nPercentWidth < nPercentHeight) ? nPercentWidth : nPercentHeight;

            //Create the output directory if it doesn't already exists
            string directoryName = Path.GetDirectoryName(outputFileName);
            if (Directory.Exists(directoryName) == false)
                Directory.CreateDirectory(directoryName);
            //Check if the file exists then delete
            if (File.Exists(outputFileName))
                File.Delete(outputFileName);

            if (percentScale < 1)
            {
                int destWidth = (int)(FullsizeImage.Width * percentScale);
                int destHeight = (int)(FullsizeImage.Height * percentScale);

                using (System.Drawing.Image thumbnailImage = FullsizeImage.GetThumbnailImage(destWidth, destHeight, null, IntPtr.Zero))
                {
                    thumbnailImage.Save(outputFileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    thumbnailImage.Dispose();
                }
            }
            else
            {
                //don't scale image up, just save original
                FullsizeImage.Save(outputFileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                FullsizeImage.Dispose();
            }
        }
        public static void CreateScaledPNG(string fileName, string outputFileName, int maxWidth, int maxHeight)
        {
            System.Drawing.Image FullsizeImage = System.Drawing.Image.FromFile(fileName);

            // Prevent using images internal thumbnail
           FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

            float nPercentWidth = ((float)maxWidth / FullsizeImage.Width);
            float nPercentHeight = ((float)maxHeight / FullsizeImage.Height);

            //use smallest percent
            float percentScale = (nPercentWidth < nPercentHeight) ? nPercentWidth : nPercentHeight;

            //Create the output directory if it doesn't already exists
            string directoryName = Path.GetDirectoryName(outputFileName);
            if (Directory.Exists(directoryName) == false)
                Directory.CreateDirectory(directoryName);
            //Check if the file exists then delete
            if (File.Exists(outputFileName))
                File.Delete(outputFileName);

            if (percentScale < 1)
            {
                int destWidth = (int)(FullsizeImage.Width * percentScale);
                int destHeight = (int)(FullsizeImage.Height * percentScale);

                using (System.Drawing.Image thumbnailImage = FullsizeImage.GetThumbnailImage(destWidth, destHeight, null, IntPtr.Zero))
                {
                    thumbnailImage.Save(outputFileName,System.Drawing.Imaging.ImageFormat.Png );
                    thumbnailImage.Dispose();
                }
            }
            else
            {
                //don't scale image up, just save original
                FullsizeImage.Save(outputFileName);
                FullsizeImage.Dispose();
            }
        }
        public static void ConvertImage(string fileName, string outputFileName)
        {
            int NewWidth = 230;
            int MaxHeight = 157;
            System.Drawing.Image FullsizeImage = System.Drawing.Image.FromFile(fileName);

            // Prevent using images internal thumbnail
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            if (FullsizeImage.Width <= NewWidth)
            {
                NewWidth = FullsizeImage.Width;
            }

            int NewHeight = FullsizeImage.Height * NewWidth / FullsizeImage.Width;
            if (NewHeight > MaxHeight)
            {
                // Resize with height instead
                NewWidth = FullsizeImage.Width * MaxHeight / FullsizeImage.Height;
                NewHeight = MaxHeight;
            }

            using (System.Drawing.Image thumbnailImage = FullsizeImage.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero))
            {
                thumbnailImage.Save(outputFileName);
                thumbnailImage.Dispose();
            }
        }
    }
}
